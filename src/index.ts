require('dotenv-flow').config();
import "reflect-metadata";
import * as express from "express";
import * as bodyParser from "body-parser";
import cors = require("cors");
import cookieParser = require('cookie-parser');

import {CLog} from "./AppHelper";

import gDB from "./InitDataSource";

import {RequestLog} from "./auth2/helper/SysLoger";

import auth2Routes from "./auth2/routes";

import fileUpload = require('express-fileupload');
const MAX_UPLOAD_FILE_SIZE = 50;
const SERVER_PORT = process.env.HTTP_PORT;





if(!process.env.SEEDCODE){
    CLog.bad("Start Server need a env SEEDCODE!");
    process.exit(1);
}



const startServer = async () => {

    try{
        await gDB.initialize()
        console.log(`"Data Source has been initialized!"`)
        CLog.ok("Data Source has been initialized!")

        // create express app
        const app = express(); // http server
        app.disable('x-powered-by');
        app.options('*', cors()) // include before other routes
        app.use(cors({exposedHeaders: ['token']}))
        // app.use(fileUpload({
        //     limits:{fileSize: MAX_UPLOAD_FILE_SIZE *1024*1024} //50mb
        // }));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({limit: `${MAX_UPLOAD_FILE_SIZE}mb`, extended: false }));
        // register express routes from defined application routes
        // register project wide routes
        // Apply the rate limiting middleware to all requests
        //enable preflight
        app.use(cookieParser())


        // todo need to optimize limiter logic
        // const limiter = rateLimit({
        //     windowMs: 2 * 60 * 1000, // 2 minutes
        //     max: 20, // Limit each IP to 20 requests per `window` (here, per 15 minutes)
        //     standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
        //     legacyHeaders: false, // Disable the `X-RateLimit-*` headers
        // })
        //
        // app.use(limiter)

        // app.use('/', (req, res) => {
        //     res.status(200).json({msg: "Good"})
        // });
        app.use('/',RequestLog(), auth2Routes);

        // setup express app here
        // ...

        // start express server
        const server = app.listen(SERVER_PORT);

        // socket io
        // await SocketManager.load(server)

        // Start Matric services
        // await Scheduler.load()

        CLog.ok(`kk`)
        console.log(`NODE_ENV is : ${process.env.NODE_ENV}.\n Express server has started on port ${SERVER_PORT}.`)

        CLog.ok(`NODE_ENV is : ${process.env.NODE_ENV}.\n Express server has started on port ${SERVER_PORT}.`)
        if(process.env.IS_PRINT_MAIL ==='true'){
            CLog.info("Mail running under: Print Mail Mode");
        }else{
            CLog.info("Mail running under: Send Mail Mode");
        }




            // // init apiGuard essentials
            // ApiGuard.initApiGuard()
            //     .then()
            //     .catch(e =>  CLog.bad("Error Server when initialize APIGuard...", e.message))
            // CourseTracker.initTrackerRedis()
            //     .then()
            //     .catch(e =>  CLog.bad("Error Server when initialize initTrackerRedis...", e.message))

        // try {
        //     await MessageQueueController.initEmailPaymentRedisConnection()
        //     // init payment receiver
        //     await ContractController.__redisUpdatePaymentReceiver()

        //     await initMailWorkerRedisConnection()

        //     //start mailWorker
        //     // mailWorkerController()

        // } catch (e) {
        //     CLog.bad("Error Server when initialize MMQ Service...", e.message)
        // }


    }catch (err) {
        CLog.bad("Error Server Initializing...", err)
        process.exit(1)
    }
}

startServer()
