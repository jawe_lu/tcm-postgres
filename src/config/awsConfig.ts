import { S3Client } from "@aws-sdk/client-s3";

const accessKeyId = process.env.S3_ACCESS_KEY_ID
const secretAccessKey = process.env.S3_SECRET_ACCESS_KEY

//* set s3 client
module.exports.s3Client = () => { 
    return new S3Client({
        region: 'us-east-1',      
        credentials: {
            accessKeyId,
            secretAccessKey
        }
    })
}