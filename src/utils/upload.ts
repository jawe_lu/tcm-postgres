const multer = require('multer')

const storage = multer.memoryStorage()

module.exports.MAX_UPLOAD_FILE_SIZE = 5
module.exports.upload = multer({
    storage: storage,
    limits: {
        fileSize: this.MAX_UPLOAD_FILE_SIZE * 1024 * 1024
    }
})