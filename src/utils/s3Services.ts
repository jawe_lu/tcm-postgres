import { PutObjectCommand } from "@aws-sdk/client-s3"
import { CLog } from "../AppHelper"

const {s3Client} = require("../config/awsConfig")


const s3 = s3Client()

module.exports.uploadFileToS3 = async (Bucket: any, Key: any, Body:  any, ContentType: any) => {
    try {
        if(!Bucket || !Key || !Body || !ContentType) {
            throw 'Upload File: missing params'
        }
        const params = {
            Bucket,
            Key,
            Body,
            ContentType
        }
        const command = new PutObjectCommand(params)
        const response = await s3.send(command)
        CLog.ok("Image uploaded successfully")
    } catch (err) {
        CLog.bad("File uploading Failed: ", err)        
    }
}