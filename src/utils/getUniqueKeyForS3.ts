const {UniqueNumberId} = require('unique-string-generator')
module.exports.getUniqueKeyForS3 = (fileName: string, dir='public') => {
    return `${dir}/${UniqueNumberId()}-${fileName}`
}
