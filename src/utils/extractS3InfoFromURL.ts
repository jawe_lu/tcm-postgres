module.exports.extractS3InfoFromURL = (url: string) => {
    const pattern = /^https:\/\/(.+?)\.s3\.(.+?)\.amazonaws\.com\/(.+)$/;
    const match = url.match(pattern);

    if (match) {
        return {
            bucket: match[1],
            region: match[2],
            key: match[3]
        };
    } else {
        throw new Error('Invalid S3 URL format');
    }
}