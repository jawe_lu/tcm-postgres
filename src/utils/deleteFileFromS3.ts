import { CLog } from "../AppHelper"

const {PutObjectCommand, DeleteObjectCommand} = require('@aws-sdk/client-s3')
const {s3Client} = require('../config/awsConfig')
const s3 = s3Client()

module.exports.deleteFileFromS3 = async (Bucket: string, Key: string) => {
    const params = {Bucket, Key}
    try {
        const data = await s3.send(new DeleteObjectCommand(params))
        CLog.ok("Success, file deleted")
    } catch (err) {
        CLog.bad("Error Delete File: ", err)
    }
}