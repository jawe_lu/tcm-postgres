module.exports.getS3ObjectURL = (bucket: string, region: string, key: string) => {
    return `https://${bucket}.s3.${region}.amazonaws.com/${key}`
}
