// 20231031, Jawe Lu

import {DataSource,DataSourceOptions} from "typeorm"
import {CLog, CPath, gisProduction} from "./AppHelper";
import {runSeeder, runSeeders, SeederOptions} from 'typeorm-extension';
import * as path from "path";

import {CheckEnvField} from "./auth2/controller/apiGuard/ApiGuard";
let dbReplication = false

if(!process.env.MYSQL_PRIMARY_HOST) {
    require('dotenv-flow').config();
}



// if (!process.env.MYSQL_PRIMARY_HOST || !process.env.MYSQL_PORT || !process.env.MYSQL_PRIMARY_USERNAME
//     || !process.env.MYSQL_PRIMARY_PASSWORD || !process.env.MYSQL_PRIMARY_DATABASE
//     || !process.env.DB_REPLICATION ) {
//     CLog.bad("Invalid or Missing [Primary] DB Config env");
//     process.exit(1);
// }

// if('true' === process.env.DB_REPLICATION.trim().toLowerCase()) {
//     if (!process.env.MYSQL_SLAVE_HOST || !process.env.MYSQL_SLAVE_USERNAME
//         || !process.env.MYSQL_SLAVE_PASSWORD || !process.env.MYSQL_SLAVE_DATABASE) {
//         CLog.bad("Invalid or Missing [Slave] DB Config env");
//         process.exit(1);
//     }
//     dbReplication = true
// }

// alert only
// CheckEnvField(
//     ['REDIS_DB_MMQ_INDEX', 'REDIS_DB_DEFAULT_INDEX',  'REDIS_DB_INDEX_TRACKER']
//     , true)


// const dbPort = +process.env.MYSQL_PORT
// if (isNaN(dbPort)) {
//     CLog.bad(`Invalid DB PORT:-->${dbPort}`);
//     process.exit(1);
// }

console.log(`gisProduction()`, gisProduction())
const entityPath = gisProduction() ? path.join(__dirname + '/../../build/src/auth2/entity/**/*.entity.js') :  path.join(__dirname + '/../src/auth2/entity/**/*.entity.ts')
console.log(`entityPath`, entityPath)
CLog.ok(`Env is: -->${process.env.NODE_ENV}` )
CLog.ok(`Server Path-->${__dirname}`)
CLog.ok(`Entity Path: -->${entityPath}`)
CLog.ok(`DB REPLICATION: -->${dbReplication}`)
CLog.ok(`DB Info:
[Master]-->${process.env.MYSQL_PRIMARY_HOST}, ${process.env.MYSQL_PRIMARY_DATABASE},
[Slave]-->${process.env.MYSQL_SLAVE_HOST}, ${process.env.MYSQL_PRIMARY_DATABASE}
`)

CLog.info(`Seed info: 
   ${process.env.TYPEORM_SEEDING_SEEDS} 
`)


// check UPLOADFILES_ROOT variable in .env file
// if it doesn't exist, create it based on the rule
const uploadPath = CPath.initUploadFileFolder()
CLog.info(`Server Upload file path: -->${uploadPath}`)

const options: DataSourceOptions & SeederOptions = 
// true ? 
    {                
        // connect remote tcm postgres database
        type: "postgres",
        host:  "tcm2024.ct6uk4aqihp3.us-east-1.rds.amazonaws.com",
        port:  5432,
        username:  "postgres",
        password:  "tcm2023+1",
        database:  "tcm2024",
        ssl: true,
        migrations: ["src/migration/*.ts"],
        logging: true,
        entities: ['./src/auth2/entity/**/*.entity.ts'],
        // entities: [gisProduction() ? path.join(__dirname + '/../../build/src/auth2/entity/**/*.entity.js') :  path.join(__dirname + '/../src/auth2/entity/**/*.entity.ts')],
        synchronize: true,
        subscribers: [],
    } 

    // {                
    //     // connect remote tcm postgres database
    //     type: "postgres",
    //     host:  "dpg-clj7hspll56s73d3fmlg-a.ohio-postgres.render.com",
    //     port:  5432,
    //     username:  "tcm_psql_c2ci_user",
    //     password:  "jEMAXSp5MqJO110BThcBIcoAOWclDy3o",
    //     database:  "tcm_psql_c2ci",
    //     ssl: true,
    //     migrations: ["src/migration/*.ts"],
    //     logging: true,
    //     entities: ['./src/auth2/entity/**/*.entity.ts'],
    //     // entities: [gisProduction() ? path.join(__dirname + '/../../build/src/auth2/entity/**/*.entity.js') :  path.join(__dirname + '/../src/auth2/entity/**/*.entity.ts')],
    //     synchronize: true,
    //     subscribers: [],
    // } 

    // :

    // {
    //     // connect local postgres database
    //     type: "postgres",
    //     host:  "localhost",
    //     port:  5432,
    //     username:  "postgres",
    //     password:  "Woaihjj1314",
    //     database:  "postgres",
    //     ssl: false,
    //     migrations: ["src/migration/*.ts"],
    //     logging: true,
    //     entities: [gisProduction() ? path.join(__dirname + '/../../build/src/auth2/entity/**/*.entity.js') :  path.join(__dirname + '/../src/auth2/entity/**/*.entity.ts')],
    //     synchronize: true,
    //     subscribers: [],


    // }
const gDB = new DataSource(options);
export default gDB
