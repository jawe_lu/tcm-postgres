import {
    Column,
    Entity,
    Generated, Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany, OneToOne,
    Timestamp,
    Unique
} from "typeorm";
import {IsEmail, IsString, Length, Matches, Max, MaxLength, Min, MinLength, Validate} from "class-validator";
import {BaseTemplate} from "./base";

import {User} from "./user.entity";


@Entity('tcm_clinic')
export class Clinic extends BaseTemplate {


    @Column()
    name: string

    @Column({type: 'simple-array', nullable: true})
    logoUrl: string[]

    @Column()
    description: string

    //contact infomation
    @Column()
    @IsEmail()
    email: string
 
    @Column()
    phone: string

    @Column()
    wechatQRCode: string

    //location infomation
    @Column({nullable: true, default: ''})
    zipcode: string
    
    @Column({nullable: true, default: ''})
    address: string
    
    @Column({nullable: true, default: ''})
    city: string
    
    @Column({nullable: true, default: ''})
    country: string

    //relations
    @ManyToOne(type => User, user => user.id)
    clinicAdmin: User

    @OneToMany(type => User, user => user.id)
    doctors: User[]

    


}