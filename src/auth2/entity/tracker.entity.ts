import {Column, Entity, JoinColumn, ManyToOne, OneToOne} from 'typeorm'
import {BaseTemplate} from './base'
import {IsBoolean, MaxLength} from 'class-validator'
import {User} from './user.entity'
// import {CampaignPoster} from './campaign_poster.entity'
// import {FormSubmission} from './form_submission.entity'
// import {QRCode} from './qrcode.entity'
// import {Contract} from './contract.entity'
// import {FormCollaboration} from './form_collaboration.entity'

@Entity('tcm_tracker')
export class Tracker extends BaseTemplate {
    @Column()
    @MaxLength(255)
    source: string

    @Column()
    @MaxLength(255)
    ip: string

    @Column('text')
    userAgentRaw: string

    

    @Column({nullable: true})
    @MaxLength(255)
    browser: string

    @Column({nullable: true})
    @MaxLength(255)
    device: string

    @Column({nullable: true})
    @MaxLength(255)
    country: string

    @Column({nullable: true})
    @MaxLength(255)
    city: string

    @Column({nullable: true})
    @MaxLength(255)
    postalCode: string

    @Column({nullable: true})
    @MaxLength(255)
    address: string

    @Column({nullable: true})
    @MaxLength(255)
    latitude: string

    @Column({nullable: true})
    @MaxLength(255)
    longitude: string

    @Column({default: false})
    @IsBoolean()
    isSkip: boolean

    // Ids to track
    @ManyToOne(type => User, u => u.id, {nullable: true})
    @JoinColumn({name: 'userId'})
    user: User

    // @ManyToOne(type => CampaignPoster, p => p.id, {nullable: true})
    // @JoinColumn({name: 'posterId'})
    // poster: CampaignPoster

    // @ManyToOne(type => QRCode, qr => qr.id, {nullable: true})
    // @JoinColumn({name: 'qrCodeId'})
    // qrCode: QRCode

    // @ManyToOne(type => FormSubmission, fs => fs.id, {nullable: true})
    // @JoinColumn({name: 'formSubmissionId'})
    // formSubmission: FormSubmission

    // @ManyToOne(type => Contract, c => c.readTracking, {nullable: true})
    // @JoinColumn({name: 'contractReadId'})
    // contractRead: Contract

    // @Column({nullable: true})
    // formCollaborationId: number
    // @ManyToOne(type => FormCollaboration, fc => fc.id, {nullable: true})
    // @JoinColumn({name: 'formCollaborationId'})
    // formCollaboration: FormCollaboration
    // @ManyToOne(type=>CampaignPosterDownloads, cpd=>cpd.id, { nullable: true })
    // @JoinColumn({ name: 'posterDownloadId' })
    // posterDownload: CampaignPosterDownloads
}
