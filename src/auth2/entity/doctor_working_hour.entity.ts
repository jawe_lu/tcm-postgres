import {Column, Entity, JoinColumn, ManyToOne} from 'typeorm'
import { BaseTemplate } from './base'
import {User} from './user.entity'

@Entity('tcm_doctor_working_hour')
export class DoctorWorkingHour extends BaseTemplate {
    // one doctor can have many days' working hour
    // user => user.doctorWorkingHours doctorWorkingHours should be reverse relation's name
    @ManyToOne(() => User, user => user.doctorWorkingHours, {onDelete: 'CASCADE'})
    // @JoinColumn({name:'doctorWorkingHours'})
    doctorId: User

    @Column()
    date: Date

    @Column()
    startTime: string
    
    @Column()
    endingTime: string

    // interval Time Between Appointments
    @Column()
    intervalTime: number


}