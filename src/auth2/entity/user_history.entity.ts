import {Column, Entity, JoinColumn, ManyToOne, OneToMany} from 'typeorm';
import {BaseTemplate} from './base';
import {User} from './user.entity';
import {MaxLength} from 'class-validator';


@Entity('tcm_user_history')
export class UserHistory extends BaseTemplate {

    @ManyToOne(type=>User, usr=>usr.history, { onDelete: "CASCADE" })
    @JoinColumn({ name: 'userId' })
    user: User

    @Column()
    @MaxLength(255)
    ip: string

    @Column('text')
    userAgentRaw: string

    @Column({ nullable: true })
    @MaxLength(255)
    browser: string

    @Column({ nullable: true })
    @MaxLength(255)
    device: string

    @Column({ nullable: true })
    @MaxLength(255)
    country: string

    @Column({ nullable: true })
    @MaxLength(255)
    city: string

    @Column({ nullable: true })
    @MaxLength(255)
    postalCode: string

    @Column({ nullable: true })
    @MaxLength(255)
    address: string

    @Column({ nullable: true })
    @MaxLength(255)
    latitude: string

    @Column({ nullable: true })
    @MaxLength(255)
    longitude: string

    @Column({ nullable: true })
    @MaxLength(255)
    referer: string

    @Column({ nullable: true })
    @MaxLength(255)
    language: string

    @Column({ nullable: true })
    @MaxLength(255)
    sid: string
}
