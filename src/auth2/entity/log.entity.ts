import {BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity('tcm_log')
export class Log extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({select: false})
    @CreateDateColumn()
    createdAt: Date

    @Column({nullable: true})
    type: string

    @Column({nullable: true})
    action: string

    @Column({nullable: true})
    ip: string

    @Column({nullable: true})
    device: string

    @Column({nullable: true})
    browser: string

    @Column({nullable: true})
    url: string

    @Column('text', {nullable: true})
    error: string

    @Column('text', {nullable: true})
    body: string

    @Column('text', {nullable: true})
    files: string

    @Column({nullable: true})
    uid: string
}
