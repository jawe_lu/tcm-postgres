import {
    Column,
    Entity,
    Generated, Index, JoinColumn,
    ManyToOne,
    OneToMany, OneToOne,
    Timestamp,
    Unique
} from "typeorm";
import {IsEmail, IsString, Length, Matches, Max, MaxLength, Min, MinLength, Validate} from "class-validator";
import {BaseTemplate} from "./base";

import {User} from "./user.entity";

export enum DurationSelection {
    THIRTY = 30,
    FOURTYFIVE = 45,
    SIXTY = 60,
    NINETY = 90
}
@Entity('tcm_appointment')
export class Appointment extends BaseTemplate {

    @Column()
    timeZone: string

    @Column({type: 'date'})
    date: string

    @Column({type: 'time'})
    time: string

    @Column({type: "enum", enum: DurationSelection, default: DurationSelection.THIRTY})
    duration: number

    @Column({nullable: true})
    attachment: string

    @ManyToOne(type => User, usr => usr.appointmentForDoctor)
    @JoinColumn({ name: "userId2"})
    doctor: User

    @ManyToOne(type => User, usr => usr.appointmentForPatient)
    @JoinColumn({ name: "userId1"})
    patient: User














}