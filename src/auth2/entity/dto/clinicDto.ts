import { IsEmail, Length } from "class-validator"
import { Column } from "typeorm"

export class CreateClinicDto {
    @Column()
    name: string

    @Column()
    logoUrl: string

    @Column()
    description: string

    //contact infomation
    // @Column()
    // @IsEmail()
    // @Length(5, 255)
    // email: string
 
    @Column()
    phone: string

    @Column()
    wechatQRCode: string

    //location infomation
    @Column({nullable: true, default: ''})
    zipcode: string
    
    @Column({nullable: true, default: ''})
    address: string
    
    @Column({nullable: true, default: ''})
    city: string
    
    @Column({nullable: true, default: ''})
    country: string
}