import { IsEmail, IsString, Length, Matches, MaxLength, MinLength, Validate } from "class-validator";
import { Column } from "typeorm";
import { DurationSelection } from "../appointment.entity";


export class AppointmentDto {

    @Column()
    timeZone: string

    @Column({type: 'date'})
    date: string

    @Column({type: 'time'})
    time: string

    @Column({type: "enum", enum: DurationSelection, default: DurationSelection.THIRTY})
    duration: number

}
