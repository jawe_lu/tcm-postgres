import { IsEmail, IsString, Length, Matches, MaxLength, MinLength, Validate } from "class-validator";
import { Column } from "typeorm";
import { CustomMatchPasswords } from "../../validator/validation";

export class SignUpDto {

    @Column()
    @Length(1, 50)
    firstName: string;

    @Column()
    @Length(1, 50)
    lastName: string;

    @Column()
    @IsEmail()
    @Length(5, 255)
    email: string

    @Column()
    @IsString()
    @MinLength(8)
    @MaxLength(20)
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {message: 'Minimum eight characters, at least one letter and one number'})
    password: string;

    @IsString()
    @MinLength(8)
    @MaxLength(20)
    // @Match('password', {message: 'confirmPassword should be consistent with password'})
    @Validate(CustomMatchPasswords, ['password'])
    passwordConfirm: string;

}
