/*
Author: Jawe Lu
TCM
Date: 2023-10-31`
 */

import {
    Column,
    Entity,
    Generated, Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany, OneToOne,
    Unique
} from "typeorm";
import {IsEmail, IsString, Length, Matches, Max, MaxLength, Min, MinLength, Validate} from "class-validator";
import {BaseTemplate} from "./base";
// import {Company} from "./company.entity";
// import {UserStaffInfo} from './user_staff_info.entity';
// import {CategoryMap} from './category_map.entity';
// import * as bcrypt from 'bcryptjs'
// import {UserFile} from './user_file.entity';
// import {Notes} from "./notes.entity";
import {UserHistory} from './user_history.entity';
import { CustomMatchPasswords} from "../validator/validation";
import { Tracker } from "./tracker.entity";
import { Appointment } from "./appointment.entity";
import { DoctorInfo } from "./doctor_info.entity";
import { Clinic } from "./clinic.entity";
import { DoctorWorkingHour } from "./doctor_working_hour.entity";



// import { ClassesTransferHistory } from "./classes_transfer_history.entity";
// import {ApiEnrollment} from "./olts/api_enrollment.entity";
// import {ApiKey} from "./olts/api_key.entity";
// import { CourseEnrollment } from "./olts/course_enrollment.entity";

enum UserRole {
    ADMIN = "admin",
    DOCTOR = "doctor",
    PATIENT = "patient",
    CLINICADMIN = 'clinicAdmin'
}

@Entity('tcm_user')
@Unique('uidKey',['uid'])
@Index(['email'], { unique: true })
export class User extends BaseTemplate {

    @Column()
    @Generated('uuid')
    uid:string;

    @Column({ default: false })
    isStaff: boolean

    @Column({ default: false })
    isCustomer: boolean

    @Column()
    @Length(1, 50)
    firstName: string;

    @Column()
    @Length(1, 50)
    lastName: string;

    @Column({ nullable: true, default: '' })
    @Length(1, 50)
    nickName: string;

    @Column({ nullable: true })
    @Min(1)
    @Max(150)
    age: number;

    @Column()
    @IsEmail()
    @Length(5, 255)
    email: string

    @Column()
    @IsString()
    @MinLength(8)
    @MaxLength(20)
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {message: 'Minimum eight characters, at least one letter and one number'})
    password: string;

    @IsString()
    @MinLength(8)
    @MaxLength(20)
    // @Match('password', {message: 'confirmPassword should be consistent with password'})
    @Validate(CustomMatchPasswords, ['password'])
    passwordConfirm: string;


    @Column({ type:"bytea",nullable: true, select:false })
    avatar: Buffer

    @Column({ default: "offline" })
    userStatus: string

    @Column({ nullable: true, default: 0 })
    verified: number // 0, unverified; 1, waiting ; 2, verified

    @Column({ nullable: true })
    vcode: string

    @Column({ nullable: true })
    vcodeDate: Date; // 20200506 13:32:22

    @Column({ nullable: true, default: '' })
    zipcode: string

    // address, city, country,
    @Column({ nullable: true, default: '' })
    address: string;

    @Column({ nullable: true, default: '' })
    city: string;

    @Column({ nullable: true, default: '' })
    country: string;

    @Column({ nullable: true })
    phone: string;

    @Column({ nullable: true })
    wechat: string;

    @Column({ nullable: true, default: '' })
    gitName: string;

    @Column({ nullable: true })
    gmail: string;

    //for customer
    @Column({ nullable: true })
    channel: string;

    @Column({ nullable: true })
    regCoordinate: string;

    @Column({ nullable: true })
    regDevice: string;

    @Column({ nullable: true })
    regInfo: string;

    @Column({ nullable: true })
    regQuestion: string;

    @Column({ nullable: true })
    regSource: string;

    @Column({
        type: "enum",
        enum: UserRole,
        default: UserRole.PATIENT
    })
    role: string

    // @OneToMany(type => Notes, note => note.customer)
    // notes: Notes[];

    // @ManyToOne(type=>CategoryMap, cm => cm.id, { onDelete: "SET NULL" })
    // @JoinColumn({ name: 'categoryId' })
    // category: CategoryMap

    // @ManyToOne(type => Company, company => company.users)
    // company: Company

    // @Column('text', { nullable: true })
    // companyDetails: string

    // @OneToMany(type=>User, user=>user.sales)
    // customer: User[];

    // @ManyToOne(type=>User, user=>user.id)
    // sales: User;

    @OneToMany(type=>UserHistory, h=>h.user)
    history: UserHistory[];


    @OneToMany(type=>Appointment, afd=>afd.doctor)
    appointmentForDoctor: Appointment[];

    @OneToMany(type=>Appointment, afp=>afp.patient)
    appointmentForPatient: Appointment[];

    @OneToOne(type => DoctorInfo, di => di.id, {onDelete: 'CASCADE'})
    @JoinColumn()
    doctorInfo: DoctorInfo

    // doctors and clinics: manytoone
    // one clinic can have many doctors
    @ManyToOne(type => Clinic, cl => cl.id)
    @JoinColumn()
    doctorClinic: Clinic

    // one clinicAdmin can have many clinics
    @OneToMany(type => Clinic, cl => cl.id)
    @JoinColumn()
    clinics: Clinic[]

      // one doctor can have many days' working hour
    @OneToMany(() => DoctorWorkingHour, doctorWorkingHour => doctorWorkingHour.doctorId, {onDelete: 'CASCADE'})
    // @JoinColumn()
    doctorWorkingHours: DoctorWorkingHour[]

 


    @OneToMany(type => Tracker, t => t.id, {nullable: true})
    user: User

    // @OneToMany(type => CourseTrack, ct => ct.user)
    // userTrack: CourseTrack[]


    // @OneToMany(type => ApiEnrollment, ce => ce.student)
    // enrollments: ApiEnrollment[]

    // @OneToMany(type => ApiKey, ce => ce.student)
    // apiKey: ApiKey[]
    // @OneToMany(type => Order, order => order.user)
    // orders: Order[]
    //
    // @OneToMany(type => Post, post => post.user)
    // posts: Post[]


    // Store Staff-specific data only
    // @OneToOne(type=>UserStaffInfo, { onDelete: 'SET NULL' })
    // @JoinColumn({ name: 'staffInfoId' })
    // staffInfo: UserStaffInfo

    // @OneToMany(type=>UserFile, uf => uf.user)
    // files: UserFile[]

    // hashPassword() {
    //     this.password = bcrypt.hashSync(this.password, 8)
    // }

    // @OneToMany(()=>ClassesTransferHistory, classesTransferHistory=>classesTransferHistory.transferStudentId)
    // transferStudentInfo:ClassesTransferHistory[]

    // @OneToMany(()=>ClassesTransferHistory, classesTransferHistory=>classesTransferHistory.repId)
    // repInfo:ClassesTransferHistory[]

    // @OneToMany(type => CourseEnrollment, ce => ce.student)
    // courseEnrollment: CourseEnrollment[]
}
