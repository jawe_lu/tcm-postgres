import {Column, Entity, JoinColumn, OneToMany, OneToOne} from 'typeorm'
import {BaseTemplate} from './base'
import {IsBoolean} from 'class-validator'
import {User} from './user.entity'

@Entity('tcm_doctor_info')
export class DoctorInfo extends BaseTemplate {
    @OneToOne(type => User, user => user.id, {onDelete: 'CASCADE'})
    @JoinColumn()
    userId: User

    @Column({default: false})
    @IsBoolean()
    isPartTime: boolean

    @Column({default: 15})
    breakTime: number

    @Column('enum', {enum: ['fullTime', 'partTime', 'contract'], default: 'fullTime'})
    status: string

    @Column('date', {nullable: true})
    joinDate: Date

    @Column('date', {nullable: true})
    leaveDate: Date

    @Column('text', {nullable: true})
    notes: string

    @Column()
    image: string




    

     // ['10',  '24']
    //  const hour = moment({
    //     hour: 15
    //   }).format('LT');
    // output: 3:00pm
    // @Column({type: 'simple-array', nullable: true})
    // mondayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // mondayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // tuesdayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // tuesdayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // wednesdayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // wednesdayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // thursdayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // thursdayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // fridayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // fridayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // saturdayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // saturdayLeftServiceTime: string[][]

    // @Column({type: 'simple-array', nullable: true})
    // sundayServiceTime: string[]

    // @Column({type: 'simple-array', nullable: true})
    // sundayLeftServiceTime: string[][]
}