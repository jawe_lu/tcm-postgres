import {default as axios} from 'axios'

export const apiConfig = (token: string, formDataHeaders, log) => {
    const config = {
        //returnRejectedPromiseOnError: true,
        timeout: 30000,
        baseURL: process.env.MATRIX_BASEURL,
        headers: {
            // common: {
            //     "Cache-Control": "no-cache, no-store, must-revalidate",
            //     Pragma: "no-cache",
            //     "Content-Type": "application/json",
            //     Accept: "application/json",
            // },
            apikey: process.env.MATRIX_APIKEY,
            authorization: `Bearer ${token ? token : ''}`,
            track: JSON.stringify(log)
        }
    }
    if (formDataHeaders) {
        config.headers = {...config.headers, ...formDataHeaders, track: JSON.stringify(log)}
    }
    return config
}

export const getApi = (token, formDataHeaders, log) => {
    const config = apiConfig(token, formDataHeaders, log)
    return axios.create(config)
}
