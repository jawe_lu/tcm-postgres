import {IUserAction} from '../helper/ReqContext'

const _ = require('lodash')
const moment = require('moment')

export interface IAuthCache {
    clearCache(): void
    setUser(companyId: number, userAction: IUserAction): Promise<void>
    getUsers(companyId)
    removeUser(companyId: number, userId: number)
}

export class AuthCache implements IAuthCache {
    private static instance: IAuthCache
    private authMap
    private activeUserMap

    private constructor() {
        this.authMap = new Map<number, number[]>()
        this.activeUserMap = new Map<number, IUserAction[]>()

        setInterval(this.checkOfflineUsers, 1000 * 60)
    }

    public static getInstance(): IAuthCache {
        if (!AuthCache.instance) {
            AuthCache.instance = new AuthCache()
        }
        return AuthCache.instance
    }

    private checkOfflineUsers = () => {
        for (let key of this.activeUserMap.keys()) {
            _.remove(this.activeUserMap.get(key), ua => moment(ua.ts).add(20, 'minutes').isBefore(moment()))
        }
    }

    public async setUser(companyId: number, userAction: IUserAction): Promise<void> {
        if (this.activeUserMap.has(companyId)) {
            const userList = this.activeUserMap.get(companyId)
            _.remove(userList, ua => ua.id === userAction.id)
            userList.push(userAction)
        } else {
            this.activeUserMap.set(companyId, [userAction])
        }
        //console.log(`Company ${companyId} Map Size = ${this.activeUserMap.get(companyId).length}`);
    }

    public async removeUser(companyId: number, userId: number) {
        _.remove(this.activeUserMap.get(companyId), ua => ua.id === userId)
        //console.log("User", userId,"Logout");
    }

    public getAll() {
        return this.activeUserMap
    }

    public getUsers(companyId) {
        return this.activeUserMap.get(companyId)
    }

    public clearCache(): void {
        this.authMap.clear()
    }
}
