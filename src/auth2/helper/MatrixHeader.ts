import Config from './Config'
import {getApi} from '../interceptor/api.axios'
import {Request} from 'express'
import {getAPITrack, getTrackingDetails} from './ReqContext'
import {CLog} from '../../AppHelper'

export interface IReturnedMessage {
    rs: boolean
    message: string
    data?: any
}

export const getMatrixApi = async (req: Request, formDataHeaders = null) => {
    let token = req.headers[Config.tokenHeader] as string
    const log = await getAPITrack(req)
    try {
        token = token.split(' ')[1]
    } catch (e) {
        CLog.bad('No Token', token)
        token = ''
    }
    return getApi(token, formDataHeaders, log)
}
