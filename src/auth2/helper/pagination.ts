import {ObjectType, SelectQueryBuilder} from 'typeorm'
import {buildPaginator} from 'typeorm-cursor-pagination'
import {encodeBase64} from './StringHelper'

export enum OptionKeywords {
    all = 'all',
    none = 'none',
    notNull = 'notNull'
}

export enum Order {
    ascending = 'ASC',
    descending = 'DESC'
}

export interface PaginationFilter {
    [property: string]: any
}

export interface PaginationParams {
    queryBuilder: SelectQueryBuilder<any>
    entity: ObjectType<any>
    page: number
    perPage: number
    sortCol?: string // sort table data by column
    sortOrder?: Order // sort table data by order
    searchCols?: string[] // search keywords in columns of table
    searchKey?: string //search key
    filters?: Object // All filter elements
    upperBounds?: Object
    lowerBounds?: Object
    previous?: string // before page cursor
    next?: string // after page cursor
    defaultSortCol: string
    defaultSearchCol: string
}

export const fetchPaginatedData = async (params: PaginationParams) => {
    const sortColName = (params.sortCol ? params.sortCol : params.defaultSortCol).trim()
    const attributeName = sortColName.split('.').pop() // Remove the prefix if exist
    const searchKey = params.searchKey?.trim().length > 0 ? ' LIKE "%' + params.searchKey.trim() + '%"' : null
    const searchColNames = params.searchCols.length >= 0 ? params.searchCols : [params.defaultSearchCol]

    let queryBuilder = params.queryBuilder

    if (params.lowerBounds) {
        Object.keys(params.lowerBounds).map((key, index) => {
            let val = params.lowerBounds[key]
            val = typeof val === 'string' ? val?.trim().toLowerCase() : val
            if (![undefined, null].includes(val)) {
                const tempName = 'tempLower'.concat(index.toString())
                queryBuilder = queryBuilder.andWhere(key + '> :' + tempName, {[tempName]: val})
            }
        })
    }

    if (params.upperBounds) {
        Object.keys(params.upperBounds).map((key, index) => {
            let val = params.upperBounds[key]
            val = typeof val === 'string' ? val.trim().toLowerCase() : val
            if (![undefined, null].includes(val)) {
                const tempName = 'tempUpper'.concat(index.toString())
                queryBuilder = queryBuilder.andWhere(key + '< :' + tempName, {[tempName]: val})
            }
        })
    }

    if (params.filters) {
        Object.keys(params.filters).map((key, index) => {
            //Filters
            let val = params.filters[key]
            if (Array.isArray(val)) {
                val.map(value => (typeof value === 'string' ? value?.trim() : value))
                queryBuilder = queryBuilder.andWhere(key + ' IN (:...arr)', {arr: val})
            } else {
                val = typeof val === 'string' ? val?.trim() : val
                if (val === OptionKeywords.none) {
                    //All data
                    queryBuilder = queryBuilder.andWhere(key + ' is null')
                } else if (val === OptionKeywords.notNull) {
                    queryBuilder = queryBuilder.andWhere(key + ' is not null')
                } else if (![OptionKeywords.all, undefined].includes(val)) {
                    const tempName = 'tempName'.concat(index.toString())
                    queryBuilder = queryBuilder.andWhere(key + '= :' + tempName, {[tempName]: val})
                }
            }
        })
    }

    if (searchKey) {
        searchColNames[0] = searchColNames[0] + searchKey
        queryBuilder = queryBuilder.andWhere(
            '(' +
                searchColNames.reduce(
                    (previousValue, currentValue) => previousValue + ' OR ' + currentValue + searchKey
                ) +
                ')'
        )
    }

    const allResults = await queryBuilder.getMany()
    const total = allResults.length
    // If the sort column is unique and distinct, cursor pagination is possible
    const isDistinctCol = total === new Set(allResults?.map(res => res[attributeName])).size
    if (isDistinctCol && (params.previous || params.next)) {
        const paginator = buildPaginator({
            entity: params.entity,
            paginationKeys: [attributeName],
            query: {
                limit: params.perPage,
                order: params.sortOrder,
                beforeCursor: params.previous,
                afterCursor: params.next
            }
        })
        const {data, cursor} = await paginator.paginate(queryBuilder)
        return {
            rs: true,
            message: 'OK',
            total: total,
            cursors: {
                previous: cursor.beforeCursor,
                next: cursor.afterCursor
            },
            data: data
        }
    } else {
        const data = await queryBuilder
            .orderBy({[sortColName]: params.sortOrder})
            .skip((params.page - 1) * params.perPage)
            .take(params.perPage)
            .getMany()

        // patch cursor to be sortable via date columns
        let cursors = {previous: null, next: null}
        if (data.length > 0) {
            let prevDataAttribute = data[0][attributeName]
            let nextDataAttribute = data[data.length - 1][attributeName]

            // when it's a date, it needs to be reformatted to a readable date string
            if (prevDataAttribute instanceof Date) {
                prevDataAttribute = prevDataAttribute.getTime()
                nextDataAttribute = nextDataAttribute.getTime()
            }
            cursors = {
                previous: encodeBase64([attributeName, prevDataAttribute].join(':')),
                next: encodeBase64([attributeName, nextDataAttribute].join(':'))
            }
        }
        return {
            rs: true,
            message: 'OK',
            total: total,
            cursors,
            data: data
        }
    }
}
