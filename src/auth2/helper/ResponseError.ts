import {Response} from "express";
import {ValidationError} from "class-validator";
import {writeHandledError} from "./SysLoger";

export const ResError = {
    Unauthorized:"Unauthorized",
    NotFound:"Not Found",
    Forbidden:"Forbidden",
    NotAcceptable:"Not Acceptable",
    Conflict:"Conflict",
    RequestOverSize:"Payload Too Large",
    InternalServerError:"The Server is temporarily unreachable.",
    //you can add more error as your wish
}

export type ResError = typeof ResError[keyof typeof ResError];
export type SysError =  string | ValidationError[] | Error;

export interface IErrorMessage {
    statusCode: number;
    error: string,
    message?: SysError;
}


const ErrorConfig:{[key:string]: IErrorMessage} = {
    [ResError.Unauthorized]:{
        statusCode: 401,
        message: "Invalid authorization",
        error: "Unauthorized"
    },
    [ResError.Forbidden]:{
        statusCode: 403,
        message: "Forbidden",
        error: "Forbidden"
    },
    [ResError.NotFound]:{
        statusCode: 404,
        message: "The requested resource is not available",
        error: "Not Found"
    },
    [ResError.NotAcceptable]:{
        statusCode: 406,
        message: "Not acceptable data format for DataBase require",
        error: "Not Acceptable",
    },
    [ResError.InternalServerError]:{
        statusCode: 500,
        message: "Internal Server Error",
        error: "Internal Server Error"
    },
    [ResError.Conflict]:{
        statusCode: 409,
        message: "data already exists!",
        error: "Conflict"
    },
    [ResError.RequestOverSize]:{
        statusCode: 413,
        message: "Payload too large, do not over xxx MB",
        error: "Payload Too Large"
    },
}


export const sendResError = (res: Response, statusCode: ResError, message?: SysError): Response => {
    const errorMessage = ErrorConfig[statusCode];
    // TODO: Replace logger
    // writeHandledError(res,statusCode,message);
    if(message){
        errorMessage.message = message;
    }
    return res.status(errorMessage.statusCode).send(errorMessage);
}

export enum HttpCode {
    E200 = 200,
    E201 = 201,
    E400 = 400,
    E404 = 404,
}

export enum ErrStr {
    ok = "",
    ErrNoObj = "Can not find the specific record",
    Err404 = `Invalid resource accessing :(`,
    ErrStore = "Failed to store data",
    FoundDataError =  "Fetch Data Error",
    CreateDataError =  "Create Data Error",
    UpdateDataError =  "Update Data Error",
    ErrMissingParameter = "Missing parameter",
    ErrServerMaintenance = "Server is Under Maintenance",
    ErrDeleteData = "Delete Data error"
}

export class Err {
    data: any;
    code: number;
    msg: string;

    constructor(
        code: HttpCode = HttpCode.E200,
        msg: string = ErrStr.ok,
        data = null
    ) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }
}