import * as express from "express";
import {NextFunction, Request, Response} from "express";
import Config from "./Config";
import {TokenPayload} from "../controller/Auth2Controller";
import * as jwt from "jsonwebtoken";
import {UploadedFile} from "express-fileupload";
import {Log} from "../entity/log.entity";
import {ResError, SysError} from "./ResponseError";
import {AuthCache} from "../cache/AuthCache";
import gDB from "../../InitDataSource";
import {CLog} from "../../AppHelper";

export function RequestLog(): express.RequestHandler {
    return async (req: Request, res: Response, next: NextFunction) => {

        next();
        if (!process.eventNames().includes('unhandledRejection')) {
            process.on('unhandledRejection', (err, promise) => {
                writeCatchUnHandledError(req, err);
                console.log('Unhandled Rejection at:', promise, 'reason:', err)
                // CLog.bad(promise.catch(what => {
                //     CLog.bad('Error in writeCatchUnhanldedError', what);
                // }));
            });
        }
        //set off log everything
        //writeRemoteAction(req);

    }
}

export interface IUserAction {
    device: string;
    browser: string;
    ip: string;
    url: string;
    uid: string;
    body: string;
    action: string;
    files: string;
    type: string;

    error?: string;
}


export const writeHandledError = async (res: Response, statusCode: ResError, error: SysError): Promise<void> => {
    // const sysp = await AuthCache.getInstance().getSystemParams(1);
    // if (sysp.get("log_all_error2db") !== 'Y') {
    //     return;
    // }
    try {

        let readAbleError;
        if (error instanceof Error) {
            readAbleError = JSON.stringify(error.stack);
        }
        if (typeof error === 'string') {
            readAbleError = error;
        }
        if (error instanceof Array) {
            readAbleError = JSON.stringify(error)
        }

        const userAction: IUserAction = getRemoteAction(res.req);
        userAction.error = readAbleError;
        userAction.type = statusCode;
        const newLog = gDB.getRepository(Log).create(userAction);
        newLog.save()
    } catch (err) {
        CLog.bad("Save log error", err);
    }
}

export const writeCatchUnHandledError = async (req, error): Promise<void> => {

    try {
        const userAction: IUserAction = getRemoteAction(req);
        userAction.error = JSON.stringify(error.stack);
        userAction.type = "unCatchedError";
        const newLog = gDB.getRepository(Log).create(userAction);
        newLog.save()
    } catch (err) {
        CLog.bad("Save log error", err);
    }
}

export const writeRemoteAction = async (req: Request): Promise<void> => {
    // const sysp = await AuthCache.getInstance().getSystemParams(1);
    // if (sysp.get("log_all_http2db") !== 'Y') {
    //     return
    // }
    if (req.originalUrl === '/auth/login') {
        return;
    }
    if(req.originalUrl.indexOf('/asset/images')>-1){
        return;
    }
    const userAction: IUserAction = getRemoteAction(req);

    const newLog = gDB.getRepository(Log).create(userAction);
    try {
        newLog.save()
    } catch (err) {
        CLog.bad("Save log error", err);
    }


}

export const getRemoteAction = (req: Request): IUserAction => {


    const userAgent = req.rawHeaders[req.rawHeaders.lastIndexOf('User-Agent') + 1]

    let token = req.headers[Config.tokenHeader] as string;
    token = token ? token.substring(7) : '';
    let jwtPlayload: TokenPayload = null
    let User_uid = '';
    if (token) {
        try {
            jwtPlayload = <TokenPayload>jwt.verify(token, Config.seedPassword);
            const {uid} = jwtPlayload;
            User_uid = uid ? uid : '';
        } catch (e) {
            //CLog.bad("token verify error");
        }
    }
    let uploadFiles = {};
    if (req.files) {
        for (let key of Object.keys(req.files)) {
            if (req.files[key] && !Array.isArray(req.files[key])) {
                const uploadFile: UploadedFile = req.files[key] as any as UploadedFile;
                uploadFiles[key] = {
                    name: uploadFile.name,
                    size: uploadFile.size,
                    type: uploadFile.mimetype,
                }
            }
        }
    }

    return {
        type: "http",
        device: userAgent.substring(userAgent.indexOf('(') + 1, userAgent.indexOf(')')),
        browser: userAgent.substring(userAgent.lastIndexOf(')') + 2).split(' ')[0],
        ip: req.ip,
        url: req.originalUrl,
        uid: User_uid,
        body: req.body ? JSON.stringify(req.body) : '',
        action: req.method,
        files: Object.keys(uploadFiles).length > 0 ? JSON.stringify(uploadFiles) : '',
    }

}

