export const SMSPhoneChecker = value => value && value.match(/^(00|\+)[1-9]{1}([0-9][\s]*){9,16}$/)

export const EmailChecker = value => value && value.match(/^\S+@\S+\.\S+$/)

export const extensionRemover = value => (typeof value === 'string' ? value.split('.').slice(0, -1).join() : '')

export const encodeBase64 = (str: string) => {
    return Buffer.from(str, 'utf-8').toString('base64')
}

export const dotConcat = (entity: string, property: string) => {
    return [entity, property].join('.')
}

export const dotConcatList = (entityList: {entity: string; property: string}[]) =>
    entityList.map(entity => [entity.entity, entity.property].join('.'))
