import axios from 'axios'

const API_KEY = '58bff50a9983464c9e2036a5b7e8e8fc'
const BASE_URL = 'https://api.opencagedata.com/geocode/v1/json'

export const geocode = async (lat, lon) => {
    try {
        const res = await axios.get(`${BASE_URL}?key=${API_KEY}&q=${lat}+${lon}`)
        if (res.data) {
            return res.data?.results[0].components
        } else {
            return {}
        }
    } catch (e) {
        return {}
    }
}
