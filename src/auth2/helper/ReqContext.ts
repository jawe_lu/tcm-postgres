import {Request} from 'express'
import {Reader} from '@maxmind/geoip2-node'
import {geocode} from './OpenCageApi'
import UAParser = require("ua-parser-js")


import {Tracker} from '../entity/tracker.entity'
import * as moment from 'moment'
import gDB from '../../InitDataSource'
import {CLog} from '../../AppHelper'

export enum OnlineUserStatus {
    Online,
    Offline,
    SignOut,
    Init
}

export interface IUserAction {
    device: string
    browser: string
    ip: string
    url: string
    ts: Date
    status: OnlineUserStatus

    id?: number
    name?: string
}
export const getRemoteAction = (req: Request): IUserAction => {
    const userAgent = req.rawHeaders[req.rawHeaders.lastIndexOf('User-Agent') + 1]
    return {
        device: userAgent.substring(userAgent.indexOf('(') + 1, userAgent.indexOf(')')),
        browser: userAgent.substring(userAgent.lastIndexOf(')') + 2).split(' ')[0],
        ip: req.ip.substring(req.ip.lastIndexOf(':') + 1),
        url: req.originalUrl,
        ts: new Date(),
        status: OnlineUserStatus.Init
    }
}

export const getAPITrack = async (req: Request) => {
    const userAgent = req.headers['user-agent']
    const ua = UAParser(userAgent)
    return {
        device: `${ua.os.name} ${ua.os.version}`,
        browser: `${ua.browser.name}/${ua.browser.version}; ${ua.engine.name}/${ua.engine.version}`,
        ip: (req.headers['x-real-ip'] || req.ip.substring(req.ip.lastIndexOf(':') + 1)).toString()
    }
}

export const getTrackingDetails = async (req: Request) => {
    const userAgent = req.headers['user-agent']
    let userIP = (req.headers['cf-connecting-ip'] || req.headers['x-real-ip'] || req.ip.substring(req.ip.lastIndexOf(':') + 1)).toString()
    const ua = UAParser(userAgent)
    let trackingData: {
        country: string
        address: string
        city: string
        userAgentRaw: string
        postalCode: string
        latitude: string
        longitude: string
        browser: string
        ip: string
        device: string
        isSkip: boolean
        referer: string
        language: string
    }

    try {
        //todo , move to redis,
        const lookup = await Reader.open('src/db/maxmind/GeoLite2-City.mmdb')


       
        const userDetails = lookup.city(userIP)
       

        const ocData = await geocode(userDetails.location?.latitude, userDetails.location?.longitude)
        const {
            building,
            road,
            village,
            neighbourhood,
            city,
            state,
            county,
            suburb,
            country,
            postcode,
            partial_postcode
        } = ocData

        const physData = {
            country: userDetails.country?.names?.en || country,
            city: userDetails.city?.names?.en || city || county || state,
            postalCode: userDetails.postal?.code || postcode || partial_postcode,
            address: [building, road, village, neighbourhood, city, county, state, suburb]
                .filter(addr => addr) // Try to piece together as much info as possible
                .join(', '),
            longitude: (userDetails.location?.longitude || '').toString(),
            latitude: (userDetails.location?.latitude || '').toString()
        }

        trackingData = {
            userAgentRaw: userAgent,
            device: `${ua.os.name} ${ua.os.version}`,
            browser: `${ua.browser.name}/${ua.browser.version}; ${ua.engine.name}/${ua.engine.version}`,
            ip: userIP,
            isSkip: false,
            referer: req?.headers?.['referer'],
            language: req?.headers?.['accept-language'],
            ...physData
        }
    } catch (e) {
        trackingData = {
            userAgentRaw: userAgent,
            device: `${ua.os.name} ${ua.os.version}`,
            browser: `${ua.browser.name}/${ua.browser.version}; ${ua.engine.name}/${ua.engine.version}`,
            ip: userIP,
            country: null,
            city: null,
            postalCode: null,
            address: null,
            longitude: null,
            latitude: null,
            isSkip: false,
            referer: req?.headers?.['referer'],
            language: req?.headers?.['accept-language']
        }
    }

    const foundPrevTrack = await gDB.getRepository(Tracker).findOne({
        where: {ip: trackingData.ip, device: trackingData.device, browser: trackingData.browser},
        order: {id: 'DESC'}
    })


// const foundPrevTrack = '' as any
console.log(`foundPrevTrack`, foundPrevTrack)


    !!foundPrevTrack && CLog.bad(foundPrevTrack)

    // Flag to skip this entry since it's made within 5 minutes by the same person
    if (foundPrevTrack && moment().diff(foundPrevTrack.createdAt, 'minutes') <= 5) {
        trackingData = {...trackingData, isSkip: true}
    }
    return trackingData
}
