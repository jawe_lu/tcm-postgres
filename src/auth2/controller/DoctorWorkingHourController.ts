import { Request,  Response} from "express";
import { BaseController } from "./BaseController";
import { CLog } from "../../AppHelper";
import { ErrStr } from "../helper/ResponseError";
import gDB from "../../InitDataSource";
import { User } from "../entity/user.entity";
import { DoctorWorkingHour } from "../entity/doctor_working_hour.entity";



class DoctorWorkingHourController extends BaseController {
    static async createWorkingHourForDoctor(req: Request, res: Response){

        try {
            const userId = req.body.validatedData

            console.log('userId', userId)
            const {date, startTime, endingTime, intervalTime, doctorId} = req.body
            const foundDoctor = gDB.getRepository(User).findOneBy({id: doctorId})

            if (!foundDoctor) {
                CLog.bad('Doctor not found', foundDoctor)
                return res.send({ rs: false, message: "Doctor not found" })
            }

            const doctorWorkingHourEntity = gDB.getRepository(DoctorWorkingHour).create(
                {
                    date, 
                    startTime, 
                    endingTime, 
                    intervalTime,
                    doctorId
                }
            )

            const doctorWorkingHour = await doctorWorkingHourEntity.save()
            return res.send({ rs: true, message: "New doctorWorkingHour created OK!!", data: doctorWorkingHour })

        }catch (err){
            CLog.bad("error", err)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }

    }
}

export default DoctorWorkingHourController