import { Request,  Response} from "express"
import { ErrStr, ResError, sendResError } from "../helper/ResponseError"
import gDB from '../../InitDataSource'
import { BaseController } from "./BaseController"
import { CLog } from "../../AppHelper"
import { Appointment } from "../entity/appointment.entity"
import { User } from "../entity/user.entity"





class AppointmentController {

    private static get repo() {
        return gDB.getRepository(Appointment)
    }

    static async createAppointment(req: Request, res: Response){
        try{
            const userId = req.body.validatedData
            console.log('userId=====>', userId)
            // 前端 整理好时区统一发数据到后端， 可以是clinic所在时区

            console.log('req.body', req.body)
            const {timeZone, date, time, duration, doctorId} = req.body
            const foundUser = await gDB.getRepository(User).findOne({
                where: {id: userId, isDelete: false, isActive: true}
            })

            console.log('foundUser===>', foundUser)

            if (!foundUser) {
                return res.send({
                rs: false,
                message: 'Patient Not Found',
            })}

            const foundDoctor = await gDB.getRepository(User)
            .createQueryBuilder("user")
            .leftJoinAndSelect("user.doctorWorkingHours", "doctorWorkingHours")
            .leftJoinAndSelect("user.doctorInfo", "doctorInfo")
            .where("user.id = :id", {id: doctorId})
            .andWhere("user.role = :role", {role:'doctor'})
            .andWhere("user.isActive = :isActive", {isActive: true})
            .andWhere("user.isDelete = :isDelete", {isDelete: false})
            .getOne()
            
            console.log('foundDoctor', foundDoctor)

            if (!foundDoctor) {
                return res.send({
                rs: false,
                message: 'Doctor Not Found',
            })}

       


            const foundAppointments = await AppointmentController.repo.find({where: {doctor: doctorId, date: date}})

         
            // create an appointment
         
            const appointmentEntity = AppointmentController.repo.create({
                doctor: foundDoctor,
                patient: foundUser,
                timeZone,
                date,
                time,
                duration,
            })

            // check is doctorid user's role is doctor
            const saved = await appointmentEntity.save()
            if(saved){
                return res.send({rs: true, message: 'appointment created successfully', data: saved})
            } else {
                return res.send({rs: false, message: 'appointment cannot be created'})
            }
        }catch(e){
            CLog.bad("error", e)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }
}


export default AppointmentController