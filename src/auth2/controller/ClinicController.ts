import { Request,  Response} from "express"
import { ErrStr, ResError, sendResError } from "../helper/ResponseError"
import gDB from '../../InitDataSource'
import { BaseController } from "./BaseController"
import { CLog } from "../../AppHelper"
import { Clinic } from "../entity/clinic.entity"
import { User } from "../entity/user.entity"
const {getUniqueKeyForS3} = require("../../utils/getUniqueKeyForS3")
const {extractS3InfoFromURL} = require("../../utils/extractS3InfoFromURL")
const {deleteFileFromS3} = require("../../utils/deleteFileFromS3")
const {uploadFileToS3} = require("../../utils/s3Services")
const {getS3ObjectURL} = require("../../utils/getS3ObjectURL")
import {consts} from "../helper/const"


class ClinicController {
    private static get repo(){
        return gDB.getRepository(Clinic)
    }
    static async createClinic(req: Request, res: Response){
        // let logoUrlKey = undefined
        let wechatQRCodeKey = undefined
        try {
            const clinicAdminId = req.body.validatedData
            console.log('clinicAdminId', clinicAdminId)
            const {name, description, email, phone, zipcode, address, city, country} = req.body

            console.log('req.files.logoUrl', req.files.logoUrl)
            if(!req.files.logoUrl) {
                CLog.bad("Create Clinic: image required")
                return res.send({ rs: false, message: 'missing logourl' })
              }
              if(!req.files.wechatQRCode) {
                CLog.bad("Create Clinic: image required")
                return res.send({ rs: false, message: 'missing wechat QR code' })
              }
            const foundClinicAdmin = await gDB.getRepository(User).findOne({
                where: {id: clinicAdminId, isDelete: false, isActive: true}
            })

            if (!foundClinicAdmin) {
                return res.send({
                rs: false,
                message: 'Clinic Admin Not Found',
            })}



            const logoUrls = req.files.logoUrl as any
           let uploadedFileLogoUrls = await Promise.all(logoUrls.map( async(ele: any, index: any) => {
            
                    const type = logoUrls[index].mimetype
                    const file = logoUrls[index].buffer
                    const logoUrlKey = getUniqueKeyForS3(logoUrls[index].originalname, 'clinics/logoUrl')
                    await uploadFileToS3(consts.Bucket, logoUrlKey, file, type) 
                 const uploadedFileLogoUrl = getS3ObjectURL(consts.Bucket, consts.Region, logoUrlKey)
                 return uploadedFileLogoUrl
                }));

            wechatQRCodeKey = getUniqueKeyForS3(req.files.wechatQRCode[0].originalname, 'clinics/wechatQRCode')
            const type1 = req.files.wechatQRCode[0].mimetype
            const file1 = req.files.wechatQRCode[0].buffer
            await uploadFileToS3(consts.Bucket, wechatQRCodeKey, file1, type1)  
            const uploadedFileWechatQRCode = getS3ObjectURL(consts.Bucket, consts.Region, wechatQRCodeKey)
    
            const clinicEntity = ClinicController.repo.create({
                name,
                description,
                email,
                phone,
                zipcode,
                address,
                city,
                country,
                clinicAdmin: foundClinicAdmin,
                logoUrl: uploadedFileLogoUrls,
                wechatQRCode: uploadedFileWechatQRCode
            })
    
            const savedClinic = await clinicEntity.save()
            if(savedClinic){
                return res.send({rs: true, message: 'clinic created successfully', data: savedClinic})
            } else {
                return res.send({rs: false, message: 'clinic cannot be created'})
            }
        }catch(e){
            CLog.bad("error", e)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }

    static async deleteClinicById(req: Request, res: Response){
        try{
            const {clinicId} = req.body
            const clinicAdminId = req.body.validatedData
            const foundClinic = await ClinicController.repo
            .createQueryBuilder("clinic")
            .leftJoinAndSelect("clinic.clinicAdmin", "clinicAdmin")
            .where("clinic.id = :id", {id: clinicId})
            .andWhere("clinic.isActive = :isActive", {isActive: true})
            .andWhere("clinic.isDelete = :isDelete", {isDelete: false})
            .andWhere("clinicAdmin.id = :clinicAdminId", {clinicAdminId: clinicAdminId})
            .getOne()

            if (!foundClinic) {
                return res.send({
                rs: false,
                message: 'Clinic Not Found',
            })}

                // delete image from s3
            Promise.all(foundClinic.logoUrl.map(async(ele) => {
                const s3Params1 = extractS3InfoFromURL(ele)
                await deleteFileFromS3(s3Params1.bucket, s3Params1.key)
            }))

            // const s3Params1 = extractS3InfoFromURL(foundClinic.logoUrl)
            // await deleteFileFromS3(s3Params1.bucket, s3Params1.key)

            const s3Params2 = extractS3InfoFromURL(foundClinic.wechatQRCode)
            await deleteFileFromS3(s3Params2.bucket, s3Params2.key)

            const deletedClinic = await ClinicController.repo.delete({id: clinicId})

            if(deletedClinic){
                return res.send({rs: true, message: 'clinic deleted successfully', data: deletedClinic})
            } else {
                return res.send({rs: false, message: 'clinic cannot be deleted'})
            }

        }catch(err){
            CLog.bad("error", err)
            return res.send({ rs: false, message: ErrStr.ErrDeleteData })
        }
    }

    static async listAllClinics(req: Request, res: Response){
        try{
            const foundClinic = await gDB.getRepository(Clinic).find({
                where: { isDelete: false, isActive: true}
            })
            if (!foundClinic) {
                return res.send({
                rs: false,
                message: 'Clinic Not Found',
            })}
            return res.send({rs: true, message: 'list all active clinics', data: foundClinic})
        }catch(err){
            CLog.bad("error", err)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }


    // temporary api for jurong test server side search function
    static async listAllClinicsBySearchKey(req: Request, res: Response){
        try{
            const {searchKey} = req.body
            if(typeof(searchKey) === "undefined"){
                const foundAllClinics = await gDB.getRepository(Clinic)
                .createQueryBuilder('clinic')
                .getMany()

                return res.send({rs: true, message: 'list all active clinics', data: foundAllClinics})
            }
            const foundClinic = await gDB.getRepository(Clinic)
            .createQueryBuilder('clinic')
            .where("clinic.name like :name", {name: `%${searchKey}%`})
            .getMany()

            console.log('foundClinic', foundClinic)
            if (!foundClinic) {
                return res.send({
                rs: false,
                message: 'Clinic Not Found',
            })}
            return res.send({rs: true, message: 'list all active clinics', data: foundClinic})
        }catch(err){
            CLog.bad("error", err)
            return res.send({ rs: false, message: ErrStr.FoundDataError })
        }
    }

    static async listClinics(req: Request, res: Response) {
        try{
            const {searchKey, currentPage, pageSize} = req.body
            let query = await ClinicController.repo
            .createQueryBuilder('clinics')
            .where("clinics.isDelete=false")
            .andWhere("clinics.isActive=true")
            if(searchKey) {
                query = await query.andWhere("clinics.name like :name", {name: `%${searchKey}%`})
            }

            const totalData = await query.getCount()

            if(currentPage && pageSize) {
                query = query.skip((currentPage - 1) * pageSize).take(pageSize)
            }                

            const clinicList = await query.getMany()

            const totalPage = Math.ceil(totalData / pageSize)

            const data = {
                totalData,
                totalPage,
                currentPage,
                pageSize,
                data: clinicList
            }

            return res.send({rs: true, message: "Get Clinic List Seccessfully!", data})
        } catch(err) {
            CLog.bad("error: ", err)
            return res.send({rs: true, message: "error in fetch: doctor list", data: err})
        }
    }
}

export default ClinicController




