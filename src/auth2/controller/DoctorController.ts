import { Request,  Response} from "express"
import { ErrStr, ResError, sendResError } from "../helper/ResponseError"
import gDB from '../../InitDataSource'
import { BaseController } from "./BaseController"
import { CLog } from "../../AppHelper"
import { Clinic } from "../entity/clinic.entity"
import { User } from "../entity/user.entity"
import {DoctorInfo} from "../entity/doctor_info.entity"
const {getUniqueKeyForS3} = require("../../utils/getUniqueKeyForS3")
const {uploadFileToS3} = require("../../utils/s3Services")
const {getS3ObjectURL} = require("../../utils/getS3ObjectURL")
import bcryptjs = require ("bcryptjs")


let region = 'us-east-1'
let bucket = 'tcm2025'


class DoctorController {
    private static get repo() {
        return gDB.getRepository(User)
    }

    static async createDoctor(req: Request, res: Response){
        let key = undefined
        try {
            const {firstName, lastName, password, passwordConfirm, email, age, role, clinicId,
            isPartTime, status, joinDate, leaveDate, notes} = req.body
            if(!req.file) {
                CLog.bad("Create Doctor: image required")
                return res.send({ rs: false, message: 'missing doctor image' })
              }
            const findClinic = await gDB.getRepository(Clinic).findOneBy({id: clinicId})
            if (!findClinic) {
                CLog.bad('Clinic not found', findClinic)
                return res.send({ rs: false, message: "Clinic not found" })
            }
            const findUser = await DoctorController.repo.findOneBy({ email })
            if (findUser) {
                CLog.bad('Email has been used', findUser)
                return res.send({ rs: false, message: "Email has been used" })
            } else {
                if(password === passwordConfirm){
                    key = getUniqueKeyForS3(req.file.originalname, 'doctors/images')
                    const type = req.file.mimetype
                    const file = req.file.buffer
                    await uploadFileToS3(bucket, key, file, type)  
                    const uploadedFile = getS3ObjectURL(bucket, region, key)
                    const doctorInfoEntity = gDB.getRepository(DoctorInfo).create({
                        isPartTime, 
                        status,
                        joinDate,
                        leaveDate,
                        notes,
                        image: uploadedFile,
                    })
                    const savedDoctorInfo = await doctorInfoEntity.save()
                    const newDoctor = await DoctorController.repo.create({
                    email,  lastName, firstName,
                    password: await bcryptjs.hash(password, 12),
                    age,
                    role, 
                    doctorClinic: findClinic,
                    doctorInfo: savedDoctorInfo
                }).save()
                return res.send({ rs: true, message: "New doctor created OK!!", data: newDoctor })
                }else{
                    CLog.bad('Please make sure password and confirmPassword the same')
                    return res.send({ rs: false, message: "Different passwords" })
                }
            }
        }catch(e){
            CLog.bad("error", e)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }

    static async listAllDoctorsFromOneClinic(req: Request, res: Response){
        const {clinicId} = req.body
        if(typeof clinicId === "undefined"){
            const foundAllDoctors = await DoctorController.repo.createQueryBuilder('doctors')
            .leftJoinAndSelect("doctors.doctorClinic", "doctorClinic")
            .leftJoinAndSelect("doctors.doctorInfo", "doctorInfo")
            .leftJoinAndSelect("doctors.doctorWorkingHours", "doctorWorkingHours")
            .getMany()
            return res.send({rs: true, message: 'fetch all doctors', data: foundAllDoctors})
        }
        const foundClinic = await gDB.getRepository(Clinic).findOne({
            where: {id: clinicId, isDelete: false, isActive: true}
        })

        if (!foundClinic) {
            return res.send({
            rs: false,
            message: 'Clinic Not Found',
        })}

        const foundDoctors = await DoctorController.repo.createQueryBuilder('doctors')
        .leftJoinAndSelect("doctors.doctorClinic", "doctorClinic")
        .leftJoinAndSelect("doctors.doctorInfo", "doctorInfo")
        .leftJoinAndSelect("doctors.doctorWorkingHours", "doctorWorkingHours")
        .where("doctorClinic.id = :id", {id: clinicId})
        .getMany()

        if (!foundDoctors) {
            return res.send({
            rs: false,
            message: 'Clinic Not Found',
        })}
        return res.send({rs: true, message: 'fetch all doctors from one specific clinic 🚀👌', data: foundDoctors})
    }


        // temporary api for jurong test server side search function
        static async listAllDoctorsBySearchKey(req: Request, res: Response){
            try{
                const {searchKey} = req.body
                if(typeof(searchKey) === "undefined"){
                    const foundAllDoctors =await DoctorController.repo.createQueryBuilder('doctors')
                    .leftJoinAndSelect("doctors.doctorClinic", "doctorClinic")
                    .leftJoinAndSelect("doctors.doctorInfo", "doctorInfo")
                    .leftJoinAndSelect("doctors.doctorWorkingHours", "doctorWorkingHours")
                    .where('doctors.isDelete=false')
                    .andWhere('doctors.isActive=true')
                    .andWhere("doctors.role = :role", {role: "doctor"})
                    .getMany()
    
                    return res.send({rs: true, message: 'list all active doctors', data: foundAllDoctors})
                }
                const foundDoctors = await DoctorController.repo
                .createQueryBuilder('doctors')
                .where("doctors.firstName like :name", {name: `%${searchKey}%`})
                .andWhere("doctors.role = :role", {role: "doctor"})
                .getMany()
    
                console.log('foundDoctors', foundDoctors)
                if (!foundDoctors) {
                    return res.send({
                    rs: false,
                    message: 'Doctors Not Found',
                })}
                return res.send({rs: true, message: 'list all filtered doctors', data: foundDoctors})
            }catch(err){
                CLog.bad("error", err)
                return res.send({ rs: false, message: ErrStr.FoundDataError })
            }
        }

        static async listDoctors(req: Request, res: Response) {
            try{
                const {searchKey, clinicId, currentPage, pageSize} = req.body
                let query = await DoctorController.repo
                .createQueryBuilder('doctors')
                .leftJoinAndSelect('doctors.doctorClinic', 'doctorClinic')
                .where("doctors.isDelete=false")
                .andWhere("doctors.isActive=true")
                .andWhere("doctors.role=:role", {role: "doctor"})
                if(clinicId) {
                    query = await query.andWhere("doctorClinic.id=:id", {id: clinicId})
                }
                if(searchKey) {
                    query = await query.andWhere("doctors.firstName like :name", {name: `%${searchKey}%`})
                }

                const totalData = await query.getCount()

                if(currentPage && pageSize) {
                    query = query.skip((currentPage - 1) * pageSize).take(pageSize)
                }                

                const doctorList = await query.getMany()

                const totalPage = Math.ceil(totalData / pageSize)

                const data = {
                    totalData,
                    totalPage,
                    currentPage,
                    pageSize,
                    data: doctorList
                }

                return res.send({rs: true, message: "Get Doctor List Seccessfully!", data})
            } catch(err) {
                CLog.bad("error: ", err)
                return res.send({rs: true, message: "error in fetch: doctor list", data: err})
            }
        }
    }



export default DoctorController