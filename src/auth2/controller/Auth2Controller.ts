/*
Author: Jawe Lu
TCM
Date: 2023-10-31`
 */
import { Request, Response } from "express";
import { AuthCache } from "../cache/AuthCache";
import { User } from "../entity/user.entity";
import { UserHistory } from '../entity/user_history.entity';
import { getTrackingDetails } from '../helper/ReqContext';
import { ErrStr, ResError, sendResError } from '../helper/ResponseError';
import gDB from "../../InitDataSource";
// import { CLog } from "../../AppHelper";
import { BaseController } from "./BaseController";
import jwt = require('jsonwebtoken');
import bcryptjs = require ("bcryptjs")

import _ = require('lodash');
import { CLog } from "../../AppHelper";

export interface TokenPayload {
    uid: string;
    email: string;
    name: string;
    exp?: number;
}

class Auth2Controller {

    private static get repo() {
        return gDB.getRepository(User)
    }

    //todo best
    static async getUserByUid(uid: string): Promise<User> {
        return await Auth2Controller.repo.findOne({ where: { uid, isActive: true }, loadRelationIds: true })
    }

    static async fetchOnlineUsers(req: Request, res: Response) {
        const { companyId } = req.body.relations;
        res.send(AuthCache.getInstance().getUsers(companyId));
    }
    static async fetchUser(req: Request, res: Response) {
        const id = +req.params.userId
        try {
            BaseController.isValidID(id)
            const result = await Auth2Controller.repo.findOne({ where: { id } })
            return res.send({ rs: true, data: result })
        } catch (e) {
            CLog.bad(`Error in fetchUser, id: ${id}, ${req.params.userId}`, e)
            return res.send({ rs: false, message: e.message })
        }
    }



    static async login(req: Request, res: Response) {
        try {
            const { email, password } = req.body
            const trackingDetails = await getTrackingDetails(req)
            const { ip, userAgentRaw, browser, device, country, city, postalCode, address, latitude, longitude, referer, language } = trackingDetails
            const findUser = await Auth2Controller.repo.findOneBy({ email })
            console.log('findUser', findUser)
            if (findUser) {
                if(await bcryptjs.compare(password, findUser.password)){
                    await gDB.getRepository(UserHistory).create({
                        ...trackingDetails,
                        user: findUser,
                    }).save()
                }else{
                    CLog.bad("password or password is incorrect")
                    return sendResError(res, ResError.Unauthorized);
                }
            } else {
                CLog.bad("email is not our user")
                return sendResError(res, ResError.Unauthorized);
            }
            const accessToken = jwt.sign({ user: findUser }, process.env.ACCESS_SECRET, { expiresIn: '3000s' });
            const refreshToken = jwt.sign({ user: findUser }, process.env.REFRESH_SECRET, { expiresIn: '6000s' });
            return res
                .cookie('refreshToken', refreshToken, { httpOnly: true, sameSite: 'strict', maxAge: 6000 * 1000 })
                .cookie('accessToken', accessToken, { httpOnly: true, sameSite: 'strict', maxAge: 3000 * 1000 })
                // .header('Authorization', accessToken)
                .send({ rs: true, payload: {accessToken, refreshToken, trackingDetails1: req.rawHeaders,
                    trackingDetails2: req.ip, trackingDetails3: req.headers, trackingDetails4: req.body

                }, message: "User login OK!!" });
        } catch (e) {
            CLog.bad("error", e)
            return sendResError(res, ResError.Unauthorized);
        }
    }


    static async signUp(req: Request, res: Response) {
        try {
            const { firstName, lastName, email, password, passwordConfirm } = req.body
            const trackingDetails = await getTrackingDetails(req)

            console.log('trackingDetails', trackingDetails)
            const { ip, userAgentRaw, browser, device, country, city, postalCode, address, latitude, longitude, referer, language } = trackingDetails

            const findUser = await Auth2Controller.repo.findOneBy({ email })
            if (findUser) {
                CLog.bad('Email has been used', findUser)
                return res.send({ rs: false, message: "Email has been used" })

            } else {
                if(password === passwordConfirm){
                     await Auth2Controller.repo.create({
                    email,  lastName, firstName,
                    password: await bcryptjs.hash(password, 12),
                }).save()
                return res.send({ rs: true, message: "New user created OK!!" })
                }else{
                    CLog.bad('Please make sure password and confirmPassword the same')
                    return res.send({ rs: false, message: "Different passwords" })
                }
            }
        } catch (e) {
            CLog.bad("error", e)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }

    static async createClinicAdmin(req: Request, res: Response) {
        try {
            const { firstName, lastName, email, password, passwordConfirm } = req.body
            const trackingDetails = await getTrackingDetails(req)
            const { ip, userAgentRaw, browser, device, country, city, postalCode, address, latitude, longitude, referer, language } = trackingDetails

            const findUser = await Auth2Controller.repo.findOneBy({ email })
            if (findUser) {
                CLog.bad('Email has been used', findUser)
                return res.send({ rs: false, message: "Email has been used" })

            } else {
                if(password === passwordConfirm){
                     await Auth2Controller.repo.create({
                    email,  lastName, firstName, 
                    password: await bcryptjs.hash(password, 12),
                    role: 'clinicAdmin'
                }).save()
                return res.send({ rs: true, message: "New user created OK!!" })
                }else{
                    CLog.bad('Please make sure password and confirmPassword the same')
                    return res.send({ rs: false, message: "Different passwords" })
                }
            }
        } catch (e) {
            CLog.bad("error", e)
            return res.send({ rs: false, message: ErrStr.CreateDataError })
        }
    }


    static async refresh(req: Request, res: Response){
        try{
            const refreshToken = req.cookies['refreshToken'];
            const payload: any = jwt.verify(refreshToken, process.env.REFRESH_SECRET)
            if(!payload){
                CLog.bad('unauthenticated', payload)
                return sendResError(res, ResError.Unauthorized);
            }
            const accessToken = jwt.sign({user: payload}, process.env.ACCESS_SECRET, {expiresIn: "3000s"})
             res.cookie('accessToken', accessToken, { httpOnly: true, sameSite: 'strict', maxAge: 300 * 1000 })
               .send({message: "success"})
           
        }catch(e){
            CLog.bad("error", e)
            return sendResError(res, ResError.Unauthorized);
        }
    }


    static async logout(req: Request, res: Response){
        res.cookie('accessToken', '', {maxAge: 0})
        res.cookie('refreshToken', '', {maxAge: 0})
        res.send({message: "success"})
    }




    static test(req: Request, res: Response) {
        res.send('ok')
    }
}

export default Auth2Controller
