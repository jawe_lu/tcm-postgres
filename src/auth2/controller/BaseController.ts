import {Request, Response} from 'express'
import {Entity, EntityTarget, ObjectType, QueryRunner, SelectQueryBuilder} from 'typeorm'
import gDB from '../../InitDataSource'
import {CLog} from '../../AppHelper'

export const ErrOK = -1

export class BaseController {
    static _queryRunner: QueryRunner = null
    constructor() {}

    public static isValidID(id: string | number | null | undefined): void {
        if (!id || (typeof id === 'string' && id.length === 0)) {
            throw new Error(`${id} is not valid! It is required and cannot be null or underfined or NaN`)
        }
    }

    // public set queryRun(_queryRunner: QueryRunner) {
    //     this._queryRunner = _queryRunner
    // }

    // public get queryRun() {
    //     return this._queryRunner
    // }

    public static parseID(id: string | number, needNumber = true): -1 | string {
        if (!id) {
            throw new Error(`${id} is not valid! It is required and cannot be null or underfined or NaN`)
        }

        if (needNumber && isNaN(+id)) {
            return `${id} is not a number, ${id}`
        }
        return ErrOK
    }

    // public set queryRun(_queryRunner: QueryRunner) {
    //     this._queryRunner = _queryRunner
    // }

    // public get queryRun() {
    //     return this._queryRunner
    // }

    public static tranRollbackRelease(): Promise<any> {
        if (!!BaseController._queryRunner) {
            return Promise.all([
                BaseController._queryRunner.rollbackTransaction(),
                BaseController._queryRunner.release()
            ])
        }

        return Promise.resolve(null)
    }

    public static repo(entity: EntityTarget<any>) {
        return gDB.getRepository(entity)
    }

    public static searchCols(entity: ObjectType<typeof Entity>) {
        return gDB.getMetadata(entity).ownColumns.map(col => col.propertyName)
    }

    public static queryBuilderAndLeftJoin(entity: ObjectType<typeof Entity>, property?: string[]) {
        const alias = entity.name.toLowerCase()
        let queryBuilder = BaseController.repo(entity).createQueryBuilder(alias)

        const allProperties = Object.values(gDB.getMetadata(entity).propertiesMap)
        if (property) {
            property = property.map(a => a.trim())
            property.map(p => {
                p = p.trim()
                if (!allProperties.includes(p)) {
                    throw 'property ' + p + ' is not a property of ' + entity.name
                }
                queryBuilder = queryBuilder.leftJoinAndSelect([alias, p].join('.'), p)
            })
        }
        return {queryBuilder, alias}
    }

    public static queryBuilderCustomLeftJoin(
        queryBuilder: SelectQueryBuilder<any>,
        propertyList: {property: string; alias: string}[]
    ) {
        propertyList.map(p => (queryBuilder = queryBuilder.leftJoinAndSelect(p.property, p.alias)))
        return queryBuilder
    }

    public static checkExist = async (
        entity: ObjectType<typeof Entity>,
        where: object,
        alias: string = entity.name
    ): Promise<any> => {
        const foundItem = await BaseController.repo(entity).findOne({where})
        if (!foundItem) {
            throw new Error(alias + ' does not exist')
        }
        return foundItem
    }

    public static async checkNonExist(
        entity: ObjectType<typeof Entity>,
        where: object,
        findEntityName: string = entity.name
    ) {
        const foundItem = await BaseController.repo(entity).findOne({where})
        if (foundItem) {
            throw new Error(findEntityName + ' is already exist. Data: ' + JSON.stringify(foundItem))
        }
    }

    public static deleteEntry = async (
        entity: ObjectType<typeof Entity>,
        req: Request,
        res: Response
    ): Promise<any> => {
        const {id} = req.params
        try {
            let foundEntry = await BaseController.checkExist(entity, {id, isDelete: false})
            foundEntry.isActive = false
            foundEntry.isDelete = true
            return res.send({rs: true, data: await foundEntry.save(), message: entity.name + ' has been deleted'})
        } catch (e) {
            console.log('Something went wrong during' + entity.name + 'deletion\n', e)
            return res.send({rs: false, message: e.message})
        }
    }

    public static fetchAllEntries = async (
        entity: ObjectType<typeof Entity>,
        where: object,
        relations: string[],
        res: Response
    ): Promise<any> => {
        try {
            const allEntries = await BaseController.repo(entity).find({where, relations})
            return res.send({rs: true, data: allEntries})
        } catch (e) {
            console.log(entity.name + ' fetch error\n', e.message)
            return res.send({rs: false, message: e.message})
        }
    }

    public static fetchOneEntry = async (
        entity: ObjectType<typeof Entity>,
        where: object,
        relations: string[],
        res: Response
    ): Promise<any> => {
        try {
            const entry = await BaseController.repo(entity).findOne({where, relations})
            return res.send({rs: Boolean(entry), data: entry})
        } catch (e) {
            console.log(entity.name + ' fetch error\n', e.message)
            return res.send({rs: false, message: e.message})
        }
    }
}
