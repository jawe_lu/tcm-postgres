// import {NextFunction, Request, Response} from 'express'
// import {BaseController} from '../BaseController'
// import {getTrackingDetails} from '../../helper/ReqContext'
// import {createClient} from 'redis'
// import {ApiKey} from '../../entity/olts/api_key.entity'
// import {
//     API_REDIS_HSET_FIELD,
//     DEFAULT_API_GUARD_ENCRYPT_ENCODING,
//     DEFAULT_API_GUARD_ENCRYPT_SOURCE_ENCODING,
//     DEFAULT_API_GUARD_MY_KEY_CONTENT,
//     DEFAULT_API_GUARD_RSA_KEY_SIZE
// } from './ApiGuardConfig'
// import * as moment from 'moment'
import {CLog} from '../../../AppHelper'
// import {ErrStr} from '../../helper/ResponseError'

export const CheckEnvField = (fields: string[], printAlert = false): string[] => {
    let missedFields = []
    for (let i = 0; i < fields.length; i++) {
        if (!process.env[fields[i]]) {
            printAlert && CLog.bad(`-->Missed env filed: ${fields[i]}`)
            missedFields.push(fields[i] as never)
        }
    }
    return missedFields
}

// class ApiGuard extends BaseController {
//     static async initApiGuard() {
//         try {
//             let curOption = 0
//             // check required env fields or quit
//             const res = CheckEnvField(['REDIS_URL', 'REDIS_PSWD', 'REDIS_DB_INDEX'], true)
//             if (res.length > 0) {
//                 // process.exit(1)
//                 throw new Error('Missing redis config in Env')
//             }
//             // connect redis server or quit
//             const redis = createClient({
//                 url: process.env.REDIS_URL,
//                 password: process.env.REDIS_PSWD,
//                 database: parseInt(process.env.REDIS_DB_INDEX),
//                 socket: {
//                     connectTimeout: 10000,
//                     reconnectStrategy: function (options) {
//                         if (options === 0) {
//                             CLog.bad('Can not connect to Redis, reconnecting')
//                         }
//                         CLog.bad(`Redis reconnect attempts ${options}`)
//                         if (options <= 60) {
//                             return 60 * 1000
//                         } else {
//                             // retry every 30 mins
//                             return 30 * 60 * 1000
//                         }
//                     }
//                 }
//             })
//             await redis.on('error', err =>
//                 CLog.bad(`Redis Client Error, ${err} at ${err.address} at ${moment().format('YYYY/MM/DD/hh:mm:ss')}`)
//             )
//             await redis.on('connect', msg =>
//                 CLog.ok(`Redis is connected successfully at ${moment().format('YYYY/MM/DD/hh:mm:ss')}`)
//             )
//             await redis.connect()
//             redis.isReady && CLog.ok(`using redis db: ${process.env.REDIS_DB_INDEX}`)
//             redis.isReady && CLog.ok(`redis in connected: ${process.env.REDIS_URL}`)
//             global.redis = redis
//         } catch (e) {
//             CLog.bad('redis connection failed', e)
//         }
//     }

//     private static generateNewKeyInfo(string) {
//         const NodeRSA = require('node-rsa')
//         const key = new NodeRSA(DEFAULT_API_GUARD_RSA_KEY_SIZE)
//         const temp = key.exportKey()
//         const encrypted = key.encrypt(
//             string,
//             DEFAULT_API_GUARD_ENCRYPT_ENCODING,
//             DEFAULT_API_GUARD_ENCRYPT_SOURCE_ENCODING
//         )
//         return {
//             rsaKey: temp,
//             myKey: encrypted
//         }
//     }

//     private static generateKeyContent(student) {
//         let res = ''
//         DEFAULT_API_GUARD_MY_KEY_CONTENT.map(option => {
//             res += `${student[option]} `
//         })
//         return res
//     }

//     static async genKeyInfo(student) {
//         try {
//             const keyString = this.generateKeyContent(student)
//             const foundApiKey = await super.repo(ApiKey).findOne({
//                 where: {
//                     student: {id: +student.id}
//                 }
//             })
//             let myResKey
//             if (!foundApiKey) {
//                 const {rsaKey, myKey} = this.generateNewKeyInfo(keyString)
//                 await super
//                     .repo(ApiKey)
//                     .create({
//                         student,
//                         rsaKey
//                     })
//                     .save()
//                 myResKey = myKey
//             } else {
//                 const NodeRSA = require('node-rsa')
//                 const key = new NodeRSA(DEFAULT_API_GUARD_RSA_KEY_SIZE)
//                 key.importKey(foundApiKey.rsaKey)
//                 myResKey = key.encrypt(
//                     keyString,
//                     DEFAULT_API_GUARD_ENCRYPT_ENCODING,
//                     DEFAULT_API_GUARD_ENCRYPT_SOURCE_ENCODING
//                 )
//             }
//             return myResKey
//         } catch (e) {
//             CLog.bad('genKeyInfo error', e)
//             return ''
//         }
//     }

//     static async checkRedisConnected(req: Request, res: Response, next: NextFunction) {
//         if (global.redis && global.redis.isReady) {
//             next()
//         } else {
//             return res.status(500).send({
//                 rs: false,
//                 message: ErrStr.ErrServerMaintenance
//             })
//         }
//     }

//     static async apiGuard(req: Request, res: Response, next: NextFunction) {
//         let apiIp = (req.headers['x-real-ip'] || req.ip.substring(req.ip.lastIndexOf(':') + 1)).toString()
//         try {
//             let userInfo = await getTrackingDetails(req)
//             if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
//                 return res.status(200).send({
//                     status: '200',
//                     message: 'Invalid access, Missing request Body.',
//                     case: 1,
//                     passed: false,
//                     visitorInfo: `${apiIp}`
//                 })
//             }
//             if (!req.body.mykey) {
//                 return res.status(200).send({
//                     status: '200',
//                     message: 'Invalid access, Missing apikey.',
//                     case: 2,
//                     passed: false,
//                     visitorInfo: `${apiIp}`
//                 })
//             }

//             if (!(await global.redis?.isReady)) {
//                 return res.status(200).send({
//                     status: '200',
//                     message: 'The server is temporarily unavailable,please try again later.',
//                     case: 6,
//                     passed: false
//                 })
//             }

//             const myKey = req.body.mykey
//             const initUrl = req.body.initUrl
//             const userIp = req.body.userIp
//             if ((await global.redis.exists(initUrl)) === 0) {
//                 return res.status(200).send({
//                     status: '200',
//                     message: 'Access Denied,the api has been disabled, please try again later.',
//                     case: 6,
//                     passed: false
//                 })
//             }
//             if (await global.redis.sIsMember(initUrl, userIp)) {
//                 return res.status(200).send({
//                     status: '200',
//                     passed: true
//                 })
//             }

//             if ((await global.redis.exists(myKey)) === 0) {
//                 return res.status(200).send({
//                     status: '200',
//                     message: 'Invalid apiKey, please check your apiKey and try again.',
//                     case: 3,
//                     passed: false
//                 })
//             } else {
//                 const curThreshold = await global.redis.hGet(myKey, API_REDIS_HSET_FIELD.CURRENT_THRESHOLD)
//                 const maxThreshold = await global.redis.hGet(myKey, API_REDIS_HSET_FIELD.MAX_THRESHOLD)
//                 const expireDate = await global.redis.hGet(myKey, API_REDIS_HSET_FIELD.EXPIRE_DATE)
//                 const url = await global.redis.hGet(myKey, API_REDIS_HSET_FIELD.URL)
//                 if (url !== initUrl) {
//                     return res.status(200).send({
//                         status: '200',
//                         message: 'Your key is not for this Api, please check your apiKey and try again.',
//                         case: 7,
//                         passed: false
//                     })
//                 }
//                 if (curThreshold > 0) {
//                     const expireTime = moment(expireDate)
//                     const currentTime = moment().startOf('second')
//                     if (expireTime.isAfter(currentTime)) {
//                         await global.redis.hSet(myKey, API_REDIS_HSET_FIELD.CURRENT_THRESHOLD, curThreshold - 1)
//                         await global.redis.hSet(myKey, API_REDIS_HSET_FIELD.LAST_VISIT, currentTime.format())
//                         return res.status(200).send({
//                             remaining: curThreshold - 1,
//                             status: '200',
//                             passed: true
//                         })
//                     } else {
//                         return res.status(200).send({
//                             status: '200',
//                             message: 'Access Denied, your apiKey has expired, please contact us to renew your myKey.',
//                             case: 4,
//                             passed: false
//                         })
//                     }
//                 } else {
//                     return res.status(200).send({
//                         status: '200',
//                         message: 'Access Denied, you have reached your daily access limit.',
//                         case: 5,
//                         passed: false
//                     })
//                 }
//             }
//         } catch (e) {
//             CLog.bad('ApiGuard has captured a error', e.message)
//             res.status(400).send({
//                 status: '400',
//                 message: 'ApiGuard has captured a error.' + e,
//                 passed: false
//             })
//         }
//     }
// }

// export default ApiGuard
