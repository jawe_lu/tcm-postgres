import {Router} from 'express'
import DoctorController from '../controller/DoctorController'
import ClinicController from '../controller/ClinicController'
import { DoctorWorkingHour } from '../entity/doctor_working_hour.entity'
import DoctorWorkingHourController from '../controller/DoctorWorkingHourController'
import { refreshToken, validateBody } from '../validator/validation'
import { CreateClinicDto } from '../entity/dto/clinicDto'
const {upload} = require('../../utils/upload')
const clinicAdminRoutes = Router({mergeParams: true})


clinicAdminRoutes.post('/createDoctor',upload.single("image"), DoctorController.createDoctor)

// upload.fields place will affect validatedData in req.body
clinicAdminRoutes.post('/createClinic',upload.fields([{name: 'logoUrl', maxCount: 10}, {name: "wechatQRCode", maxCount: 1}]), refreshToken(),   ClinicController.createClinic)

clinicAdminRoutes.delete('/deleteClinicById', refreshToken(),ClinicController.deleteClinicById)

clinicAdminRoutes.post('/createDoctorWorkingHour', DoctorWorkingHourController.createWorkingHourForDoctor)
export default clinicAdminRoutes