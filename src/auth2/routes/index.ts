import { Router } from "express";
import authRoutes from "./auth2.routes";
// import functionRoutes from "./function.routes";
import {authValidator, checkAuthFromMatrix, checkUserRole, refreshToken, validateBody, validateToken} from "../validator/validation";
// import noAuthRoutes from "./noAuth.routes";
// import customerRoutes from "./customer.routes";
import {Functions} from "../helper/FunctionEnum";
// import userRoutes from "./user.routes";
// import assetRoutes from "./assetRoutes";
// import StaffRoutes from "./staff.routes";
// import ApiRoutes from "./api.routes";
// import formRoutes from './form.routes';
// import categoryMapRoutes from './categoryMap.routes';
// import classesRoutes from './classes.routes';
// import productRoutes from './product.routes';
// import messageRoutes from "./message.routes";
// import campaignRoutes from './campaign.routes';
// import administrativeRoutes from './administrative.routes';
// import reviewRoutes from './review.routes';
// import googleAutoCompleteRoutes from "./googleAutoComplete";
// import qrcodeRoutes from './qrcode.routes';
// import posterMakerRoutes from './posterMaker.routes';
// import matricRoutes from './matric.routes';
// import resourcesRoutes from "./resources.routes";
// import oltsRoutes from './olts/oltsIndex';
// import messageQueue from "./messageQueue";
// import webFormRoutes from "./webForm.routes";
import {getTrackingDetails} from "../helper/ReqContext";
// import ApiGuard from "../controller/apiGuard/ApiGuard";
import {CLog} from "../../AppHelper";
import {Err, ErrStr, HttpCode} from "../helper/ResponseError";
import gDB from "../../InitDataSource";
import patientRoutes from "./patient.routes";
import clinicAdminRoutes from "./clinicAdmin.routes";
import adminRoutes from "./admin.routes";
import publicRoutes from "./public.routes";
import { CreateClinicDto } from "../entity/dto/clinicDto";
// import {Contract} from "../entity/contract.entity";
// import {convertToCents} from "../helper/PaymentHelper";




const auth2Routes = Router();

// routes prototype,
auth2Routes.use('/auth',authRoutes);
// auth2Routes.use('/asset',assetRoutes);

auth2Routes.use('/public',
//  refreshToken(),
 publicRoutes)

auth2Routes.use('/patient',

 checkUserRole('patient'),
 refreshToken(),
 patientRoutes)

auth2Routes.use('/clinicAdmin', 
checkUserRole('clinicAdmin'),
refreshToken(),
  clinicAdminRoutes)

  auth2Routes.use('/admin', 
refreshToken(),
checkUserRole('admin'),
  adminRoutes)

// set api after token
// auth2Routes.use('/api',validateToken(),ApiRoutes)


// function roles 不要加全局验证，用户需要获取菜单，但并不一定需要有function管理权力
// auth2Routes.use('/function',validateToken(),functionRoutes);


// every use has rights to update their own information
// auth2Routes.use('/user',validateToken(),userRoutes)

// 👇是可以加全局认证的
// auth2Routes.use('/customer',validateToken(),
//     customerRoutes);

// auth2Routes.use('/staff',validateToken(),
//     //authValidator(Functions.HR_Management_Employee_Management), // 这里不需要检测，除非你的二级路由很多没有单独的权限验证
//     StaffRoutes);

// auth2Routes.use('/formBuilder', validateToken(),
//     formRoutes);

// auth2Routes.use('/collaboration', webFormRoutes);

// auth2Routes.use('/posterMaker', validateToken(),
//     posterMakerRoutes);

// // Temp category use
// auth2Routes.use('/categoryMap',validateToken(),
//     categoryMapRoutes);

// auth2Routes.use('/classes',validateToken(),
//     checkAuthFromMatrix(Functions.Training),
//     classesRoutes);

// auth2Routes.use('/product',validateToken(),
//     authValidator(Functions.Product),
//     productRoutes);

// auth2Routes.use('/message',validateToken(),
//     messageRoutes);

// auth2Routes.use('/campaign',validateToken(),
//     campaignRoutes);

// auth2Routes.use('/qr',validateToken(),
//     qrcodeRoutes);

// auth2Routes.use('/administrative',
//     validateToken(),
//     administrativeRoutes);

// auth2Routes.use('/review', validateToken(),
//     reviewRoutes);

// auth2Routes.use('/matric',validateToken(),
//     matricRoutes);

// auth2Routes.use('/google',googleAutoCompleteRoutes)
// //without token

// auth2Routes.use('/',noAuthRoutes);

// auth2Routes.use('/google',
//     validateToken(),
//     checkAuthFromMatrix(Functions.Training_Resources),
//     resourcesRoutes)

// auth2Routes.use('/olts', oltsRoutes)

// auth2Routes.post('/apiGuard',ApiGuard.apiGuard)

// auth2Routes.use('/msg-queue',messageQueue)

// auth2Routes.get('/internal-upgrade_deprecated/updateContractTemp',async (req,res)=>{
//     return
//     if(req.hostname === 'localhost'){
//         try{
//             const contract_old = await gDB.query(
//                 'SELECT * FROM lms_contract_before_payment_log_update ORDER BY id'
//             );
//             const contract = await gDB.getRepository(Contract).find()
//             await Promise.all(contract.map((contract,index) => {
//                 let old_contract = contract_old.find((ele)=>{
//                     return ele.id === contract.id
//                 })
//                 if (!!old_contract){
//                     contract.priceInCent = parseInt((parseFloat(old_contract.price) * 100).toFixed(0))
//                     contract.taxAmount = !!old_contract.taxAmount ? convertToCents(old_contract.taxAmount) : old_contract.tax ? parseInt((parseFloat(old_contract.price) * 0.13 * 100).toFixed(0)) : 0
//                     contract.taxRate = !!old_contract.taxRate ? convertToCents(old_contract.taxRate) : 113
//                     contract.discount = !!old_contract.discount ? convertToCents(old_contract.discount) : 0
//                     contract.total = contract.priceInCent + contract.taxAmount - contract.discount
//                     contract.currency = !!old_contract.currency ? old_contract.currency : 1
//                     return gDB.getRepository(Contract).save(contract)
//                 }else {
//                     CLog.bad(`Contract ID : ${contract.id} does not have corresponding old_data.` )
//                 }
//             }))
//             CLog.info("contract table migrated done. 🥳🥳🥳")
//             return res.send({rs:true,message:'updateContractTemp successfully'})
//         }catch (e){
//             CLog.bad('error in updateContractTemp',e.message)
//             return res.send({rs:false,message:'error in updateContractTemp'+e.message})
//         }

//     }else{
//         return res.send({rs:false,message:'error in updateContractTemp, need to visit from localhost'})
//     }
// })

// route fallback, must always be the blowest!!!
auth2Routes.use('*', (req, res) => {
    const {baseUrl, method, query, rawHeaders} = req;
    CLog.bad(`global 404: invalid route ->>`, {baseUrl, method, query, rawHeaders})
    return res.status(HttpCode.E404).send(new Err(HttpCode.E404, ErrStr.Err404))
})

export default auth2Routes

