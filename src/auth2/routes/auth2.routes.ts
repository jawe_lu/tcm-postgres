import {Router} from 'express'
import Auth2Controller from '../controller/Auth2Controller'

// import {
//     AuthCredentialsDto,
//     ChangePasswordDto,
//     CheckResetPasswordCodeDto,
//     RequestResetPasswordDto,
//     ResetPasswordDto
// } from '../entity/dto/authCredentialsDto'
import {authValidator, validateBody, validateToken, validateTokenForAuth} from '../validator/validation'
import { SignUpDto } from '../entity/dto/authCredentialsDto'
// import {Functions} from '../helper/FunctionEnum'
// import {forwardToMatrix} from '../interceptor/http.interceptor'

const authRoutes = Router({mergeParams: true})

//authRoutes.post('/login', validateBody(AuthCredentialsDto), Auth2Controller.login)


authRoutes.post('/signUp', validateBody(SignUpDto), Auth2Controller.signUp)

authRoutes.get('/test', validateTokenForAuth(), Auth2Controller.test)

authRoutes.post('/login',
//  forwardToMatrix(true), 
 Auth2Controller.login)

 authRoutes.post('/refresh', Auth2Controller.refresh)

 authRoutes.post('/logout',
 Auth2Controller.logout)

// authRoutes.get('/logout', validateToken(), Auth2Controller.logout)

// authRoutes.post(
//     '/reqResetPassword',
//     validateBody(RequestResetPasswordDto),
//     forwardToMatrix(false, '/auth/requestResetPassword')
// )

// authRoutes.post('/checkResetPasswordCode', validateBody(CheckResetPasswordCodeDto), forwardToMatrix())
// authRoutes.put('/resetPassword', validateBody(ResetPasswordDto), forwardToMatrix())

// //authRoutes.post('/authCheck', validateToken(), Auth2Controller.authCheck)

// authRoutes.post('/authCheck', forwardToMatrix())

// authRoutes.get('/authUser/:userId', Auth2Controller.fetchUser)

// authRoutes.get(
//     '/fetchOnlineUsers',
//     validateToken(),
//     authValidator(Functions.System_Management_Online_Users),
//     Auth2Controller.fetchOnlineUsers
// )

// //authRoutes.get('/fetchAuth/:id',validateTokenWithoutCheckDB(),Auth2Controller.fetchAuth)
// authRoutes.get('/fetchAuth/:id', forwardToMatrix())
// // authRoutes.get('/fetchFunctionEnum',validateTokenWithoutCheckDB(),Auth2Controller.fetchFunctionEnum) !!!move to function.routes

// // authRoutes.get('/fetchSwitchUserList',
// //     validateToken(),
// //     authValidator(Functions.System_Management_Switch_User),
// //     Auth2Controller.getSwitchUserList
// // )

// authRoutes.get('/superLogin/:userId', forwardToMatrix())

// authRoutes.post('/changePassword', validateBody(ChangePasswordDto), forwardToMatrix(false, '/users/changePassword'))

//router.put('/change-password', Auth2Controller.changePassword)

export default authRoutes
