import {Router} from 'express'
import AppointmentController from '../controller/AppointmentController'
import DoctorController from '../controller/DoctorController'
import { validateBody } from '../validator/validation'
import { AppointmentDto } from '../entity/dto/appointmentDto'


const patientRoutes = Router({mergeParams: true})


patientRoutes.post('/createAppointment', validateBody(AppointmentDto), AppointmentController.createAppointment)

// patientRoutes.get('/listAllDoctorsFromOneClinic', DoctorController.listAllDoctorsFromOneClinic)


export default patientRoutes