import {Router} from 'express'
import AppointmentController from '../controller/AppointmentController'
import DoctorController from '../controller/DoctorController'
import { validateBody } from '../validator/validation'
import { AppointmentDto } from '../entity/dto/appointmentDto'
import ClinicController from '../controller/ClinicController'


const publicRoutes = Router({mergeParams: true})


publicRoutes.get('/listAllDoctorsFromOneClinic', DoctorController.listAllDoctorsFromOneClinic)

publicRoutes.get('/listAllClinics', ClinicController.listAllClinics)

publicRoutes.get('/listAllClinicsBySearchKey', ClinicController.listAllClinicsBySearchKey)

// listAllDoctorsBySearchKey

publicRoutes.get('/listAllDoctorsBySearchKey', DoctorController.listAllDoctorsBySearchKey)

// list doctors
publicRoutes.get("/list-doctors", DoctorController.listDoctors)

// list clinics
publicRoutes.get("/list-clinics", ClinicController.listClinics)


export default publicRoutes