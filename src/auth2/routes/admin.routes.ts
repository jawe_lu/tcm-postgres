import {Router} from 'express'
import Auth2Controller from '../controller/Auth2Controller'

const adminRoutes = Router({mergeParams: true})


adminRoutes.post('/createClinicAdmin', Auth2Controller.createClinicAdmin)


export default adminRoutes